# Satisfactory Mermaid Graph Generator

**Still in early stages of development**

This is a simple JAVA project to generate production flowchart for the game Satisfactory.

## What it is

**This is** to document your factories and **is not** to plan them from the ground up. It can do that but it need to
understand exactly what you want it to do. To ensure you get what you want you'll need to provide it with every final
output you want **and all the recipes you want to use**. If you don't, it will find a **random** recipe for that item so
you can end up flow chart that make no sense or are not very practical/optimized.

## How it's made

It is based on [mermaid](https://mermaid-js.github.io/mermaid/#/flowchart) so it can be easily embeddable in markdown.
It supports all basic [styles](https://mermaid-js.github.io/mermaid/#/flowchart?id=styling-and-classes) mermaid
supports; easily configurable.

All item/recipes are hardcoded in enums so you can easily choose them.

## But why ?

I have a satisfactory world save with a friend. I put my saves under git and commit them regularly on
a [GitLab repo](https://gitlab.com/vba-saves/satisfactory/chill-place); it is the easiest way to back them up and share
them easily wth my friend (even tho git is not made for binary; it's enough for what I do)

At some point we activated [the wiki](https://gitlab.com/vba-saves/satisfactory/chill-place/-/wikis/home) on our repo
and started documenting things there. Again it's easier to share our notes this way. And it's such a relief to have an
historic of what you made when you come back upgrading/debuging a factory.

Then I remembered I once saw
that [GitLab markdown supported some kind of flow chart](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts)
; so i dig that back. And i found mermaid ! It is so convenient and perfect for my use case of documenting factories !

At first I was writing those by hand in their live editor having the satisfactory fandom on my second screen; but lazy
as I am I couldn't stop thinking how easy it could be to automate such a redundant process.

So here it is !

**tl;dr Perfect if you have a markdown wiki documenting your world.**

## Exemple

Feel free to check the source of this README. This is the mermaid code generated from this app.

*Disclaimer: this is an old generated graph that may not be accurate*

```mermaid
	%% Computed
	graph TB
		%% 2022-49-09 07:49:04.88
		
		%% generic style
		classDef ext fill:#DDDDDD, color:#222222;
		classDef raw fill:#FBF46D, color:#222222;
		classDef tra fill:#998CEB, color:#DDDDDD;
		classDef cra fill:#B4FE98, color:#222222;
		classDef out fill:#77E4D4, color:#222222, stroke:#DDDDDD, stroke-width:2px;
		
		
		FACTORY_OUTPUT("Factory output"):::out
		
		%% Extracors definition
		M__E__MINER_RE__IRON_ORE_MINING["Miner PRODUCING 'Iron ore mining'"]:::ext
		
		%% Raw resources definition
		R__IRON_ORE>"Iron Ore"]:::raw
		
		%% All machine groups
		M__T__TODO_RT__SCREW_STANDARD["[450.0%] T__TODO USING RECIPE '[S] Screw'  |  In: 10.0  |  Out: 40.0"]:::tra
		M__T__TODO_RT__IRON_PLATE_STANDARD["[450.0%] T__TODO USING RECIPE '[S] Iron Plate'  |  In: 30.0  |  Out: 20.0"]:::tra
		M__T__TODO_RT__IRON_INGOT_STANDARD["[600.0%] T__TODO USING RECIPE '[S] Iron Ingot'  |  In: 30.0  |  Out: 30.0"]:::tra
		M__T__TODO_RT__IRON_ROD_STANDARD["[300.0%] T__TODO USING RECIPE '[S] Iron Rod'  |  In: 15.0  |  Out: 15.0"]:::tra
		M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD["[300.0%] T__TODO USING RECIPE '[S] Reinforced Iron Plate'  |  In: 30.0 - 60.0  |  Out: 5.0"]:::tra
		
		%% Intermediate items used in process
		C__SCREW(["Screw"]):::cra
		C__IRON_PLATE(["Iron Plate"]):::cra
		C__IRON_INGOT(["Iron Ingot"]):::cra
		C__IRON_ROD(["Iron Rod"]):::cra
		C__REINFORCED_IRON_PLATE(["Reinforced Iron Plate"]):::cra
		
		%% Extraction
		%% Iron ore mining
		
		M__E__MINER_RE__IRON_ORE_MINING -. "180" .-> R__IRON_ORE
		
		%% -------------------------------
		
		%% Transformation
		%% [S] Screw
		C__IRON_ROD -- "45" --> M__T__TODO_RT__SCREW_STANDARD
		
		M__T__TODO_RT__SCREW_STANDARD -. "180" .-> C__SCREW
		
		%% -------------------------------
		
		%% [S] Iron Plate
		C__IRON_INGOT -- "135" --> M__T__TODO_RT__IRON_PLATE_STANDARD
		
		M__T__TODO_RT__IRON_PLATE_STANDARD -. "90" .-> C__IRON_PLATE
		
		%% -------------------------------
		
		%% [S] Iron Ingot
		R__IRON_ORE -- "180" --> M__T__TODO_RT__IRON_INGOT_STANDARD
		
		M__T__TODO_RT__IRON_INGOT_STANDARD -. "180" .-> C__IRON_INGOT
		
		%% -------------------------------
		
		%% [S] Iron Rod
		C__IRON_INGOT -- "45" --> M__T__TODO_RT__IRON_ROD_STANDARD
		
		M__T__TODO_RT__IRON_ROD_STANDARD -. "45" .-> C__IRON_ROD
		
		%% -------------------------------
		
		%% [S] Reinforced Iron Plate
		C__IRON_PLATE -- "90" --> M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD
		C__SCREW -- "180" --> M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD
		
		M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD -. "15" .-> C__REINFORCED_IRON_PLATE
		
		%% -------------------------------
		
		%% Output
		C__REINFORCED_IRON_PLATE == "15.0" ==> FACTORY_OUTPUT
		
		
```

```
	%% Computed
	graph TB
		%% 2022-49-09 07:49:04.88
		
		%% generic style
		classDef ext fill:#DDDDDD, color:#222222;
		classDef raw fill:#FBF46D, color:#222222;
		classDef tra fill:#998CEB, color:#DDDDDD;
		classDef cra fill:#B4FE98, color:#222222;
		classDef out fill:#77E4D4, color:#222222, stroke:#DDDDDD, stroke-width:2px;
		
		
		FACTORY_OUTPUT("Factory output"):::out
		
		%% Extracors definition
		M__E__MINER_RE__IRON_ORE_MINING["Miner PRODUCING 'Iron ore mining'"]:::ext
		
		%% Raw resources definition
		R__IRON_ORE>"Iron Ore"]:::raw
		
		%% All machine groups
		M__T__TODO_RT__SCREW_STANDARD["[450.0%] T__TODO USING RECIPE '[S] Screw'  |  In: 10.0  |  Out: 40.0"]:::tra
		M__T__TODO_RT__IRON_PLATE_STANDARD["[450.0%] T__TODO USING RECIPE '[S] Iron Plate'  |  In: 30.0  |  Out: 20.0"]:::tra
		M__T__TODO_RT__IRON_INGOT_STANDARD["[600.0%] T__TODO USING RECIPE '[S] Iron Ingot'  |  In: 30.0  |  Out: 30.0"]:::tra
		M__T__TODO_RT__IRON_ROD_STANDARD["[300.0%] T__TODO USING RECIPE '[S] Iron Rod'  |  In: 15.0  |  Out: 15.0"]:::tra
		M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD["[300.0%] T__TODO USING RECIPE '[S] Reinforced Iron Plate'  |  In: 30.0 - 60.0  |  Out: 5.0"]:::tra
		
		%% Intermediate items used in process
		C__SCREW(["Screw"]):::cra
		C__IRON_PLATE(["Iron Plate"]):::cra
		C__IRON_INGOT(["Iron Ingot"]):::cra
		C__IRON_ROD(["Iron Rod"]):::cra
		C__REINFORCED_IRON_PLATE(["Reinforced Iron Plate"]):::cra
		
		%% Extraction
		%% Iron ore mining
		
		M__E__MINER_RE__IRON_ORE_MINING -. "180" .-> R__IRON_ORE
		
		%% -------------------------------
		
		%% Transformation
		%% [S] Screw
		C__IRON_ROD -- "45" --> M__T__TODO_RT__SCREW_STANDARD
		
		M__T__TODO_RT__SCREW_STANDARD -. "180" .-> C__SCREW
		
		%% -------------------------------
		
		%% [S] Iron Plate
		C__IRON_INGOT -- "135" --> M__T__TODO_RT__IRON_PLATE_STANDARD
		
		M__T__TODO_RT__IRON_PLATE_STANDARD -. "90" .-> C__IRON_PLATE
		
		%% -------------------------------
		
		%% [S] Iron Ingot
		R__IRON_ORE -- "180" --> M__T__TODO_RT__IRON_INGOT_STANDARD
		
		M__T__TODO_RT__IRON_INGOT_STANDARD -. "180" .-> C__IRON_INGOT
		
		%% -------------------------------
		
		%% [S] Iron Rod
		C__IRON_INGOT -- "45" --> M__T__TODO_RT__IRON_ROD_STANDARD
		
		M__T__TODO_RT__IRON_ROD_STANDARD -. "45" .-> C__IRON_ROD
		
		%% -------------------------------
		
		%% [S] Reinforced Iron Plate
		C__IRON_PLATE -- "90" --> M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD
		C__SCREW -- "180" --> M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD
		
		M__T__TODO_RT__REINFORCED_IRON_PLATE_STANDARD -. "15" .-> C__REINFORCED_IRON_PLATE
		
		%% -------------------------------
		
		%% Output
		C__REINFORCED_IRON_PLATE == "15.0" ==> FACTORY_OUTPUT
		
		
```

