package com.bancarelvalentin.smgg.theme.themes

import com.bancarelvalentin.smgg.mermaid.MermaidClass
import com.bancarelvalentin.smgg.mermaid.enums.Links
import com.bancarelvalentin.smgg.mermaid.enums.Shapes
import com.bancarelvalentin.smgg.theme.Theme
import java.awt.Color

@Suppress("unused")
class RandomTheme : Theme() {

    override var DEFAULT_FILL_COLOR: String? = rndColor()
    override var DEFAULT_TEXT_COLOR: String? = rndColor()
    override var DEFAULT_STROKE_COLOR: String? = rndColor()
    override var DEFAULT_STROKE_WIDTH: Int? = rndWidth()
    override var DEFAULT_STROKE_DASHES: Pair<Int, Int>? = rndDashes()
    override var EXTRACTORS_CLASS: MermaidClass? = rndClass()
    override var EXTRACTORS_SHAPE: Shapes? = rndShape()
    override var RAWS_CLASS: MermaidClass? = rndClass()
    override var RAWS_SHAPE: Shapes? = rndShape()
    override var TRANSFORMERS_CLASS: MermaidClass? = rndClass()
    override var TRANSFORMERS_SHAPE: Shapes? = rndShape()
    override var CRAFTABLES_CLASS: MermaidClass? = rndClass()
    override var CRAFTABLES_SHAPE: Shapes? = rndShape()
    override var OUTPUT_CLASS: MermaidClass? = rndClass()
    override var OUTPUT_SHAPE: Shapes? = rndShape()
    override var OUTUTS_LINK_TYPE: Links? = rndLink()
    override var INPUTS_LINK_TYPE: Links? = rndLink()
    override var FINAL_OUTPUT_LINK_TYPE: Links? = rndLink()

    private fun rndClass(): MermaidClass {
        return MermaidClass(
            fillColor = rndColor(),
            textColor = rndColor(),
            strokeColor = rndColor(),
            strokeWidth = rndWidth(),
            strokeDashes = rndDashes()
        )
    }

    private fun rndDashes(): Pair<Int, Int> {
        return Pair(rndWidth(), rndWidth())
    }

    private fun rndWidth(): Int {
        return (2..10).random()
    }

    private fun rndShape(): Shapes {
        return Shapes.values().toList().shuffled().first()
    }

    private fun rndLink(): Links {
        return Links.values().toList().shuffled().first()
    }

    private fun rndColor(): String {
        val color = Color(((Math.random() * 0x1000000).toInt()))
        val buf = Integer.toHexString(color.rgb)
        return buf.substring(buf.length - 6)
    }
}
