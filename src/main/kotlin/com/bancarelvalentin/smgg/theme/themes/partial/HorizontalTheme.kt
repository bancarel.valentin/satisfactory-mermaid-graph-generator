package com.bancarelvalentin.smgg.theme.themes.partial

import com.bancarelvalentin.smgg.mermaid.enums.Orientations
import com.bancarelvalentin.smgg.theme.Theme

@Suppress("unused")
class HorizontalTheme : Theme() {
    override var ORIENTATION: Orientations? = Orientations.LEFT_RIGHT
}
