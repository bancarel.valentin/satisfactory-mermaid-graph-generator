package com.bancarelvalentin.smgg.theme.themes

import com.bancarelvalentin.smgg.mermaid.MermaidClass
import com.bancarelvalentin.smgg.mermaid.enums.Formats
import com.bancarelvalentin.smgg.mermaid.enums.Links
import com.bancarelvalentin.smgg.mermaid.enums.Orientations

@Suppress("unused")
open class PastelTheme : DefaultShapesTheme() {
    override var ORIENTATION: Orientations? = Orientations.TOP_BOTTOM
    override var FORMAT: Formats? = Formats.MARKDOWN

    override var USER_INPUTS_CLASS: MermaidClass? = MermaidClass("user", LIGHT, DARK, null, null, null)
    override var EXTRACTORS_CLASS: MermaidClass? = MermaidClass("ext", LIGHT, DARK, null, null, null)
    override var RAWS_CLASS: MermaidClass? = MermaidClass("raw", "FFEEAD", DARK, null, null, null)
    override var TRANSFORMERS_CLASS: MermaidClass? = MermaidClass("tra", "FFAD60", DARK, null, null, null)
    override var CRAFTABLES_CLASS: MermaidClass? = MermaidClass("cra", "96CEB4", DARK, null, null, null)
    override var OUTPUT_CLASS: MermaidClass? = MermaidClass("out", "D9534F", LIGHT, "DDDDDD", 2, null)

    override var OUTUTS_LINK_TYPE: Links? = Links.DOTTED
    override var INPUTS_LINK_TYPE: Links? = Links.ARROW
    override var FINAL_OUTPUT_LINK_TYPE: Links? = Links.THICK
}
