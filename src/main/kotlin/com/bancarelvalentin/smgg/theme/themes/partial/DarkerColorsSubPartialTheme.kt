package com.bancarelvalentin.smgg.theme.themes.partial

import com.bancarelvalentin.smgg.mermaid.MermaidClass
import com.bancarelvalentin.smgg.theme.Theme

@Suppress("unused")
class DarkerColorsSubPartialTheme : Theme() {
    override var EXTRACTORS_CLASS: MermaidClass? = MermaidClass(null, null, "000", null, null, null)
    override var RAWS_CLASS: MermaidClass? = MermaidClass(null, null, "000", null, null, null)
    override var TRANSFORMERS_CLASS: MermaidClass? = MermaidClass(null, null, "000", null, null, null)
    override var CRAFTABLES_CLASS: MermaidClass? = MermaidClass(null, null, "000", null, null, null)
    override var OUTPUT_CLASS: MermaidClass? = MermaidClass(null, null, "000", null, null, null)
}
