package com.bancarelvalentin.smgg.theme.themes

import com.bancarelvalentin.smgg.mermaid.enums.Shapes
import com.bancarelvalentin.smgg.theme.Theme

@Suppress("unused")
open class DefaultShapesTheme : Theme() {
    override var USER_INPUTS_SHAPE: Shapes? = Shapes.CYLINDRICAL
    override var EXTRACTORS_SHAPE: Shapes? = Shapes.RHOMBUS
    override var RAWS_SHAPE: Shapes? = Shapes.ASSYMETRIC
    override var TRANSFORMERS_SHAPE: Shapes? = Shapes.SQUARE
    override var CRAFTABLES_SHAPE: Shapes? = Shapes.STADIUM
    override var OUTPUT_SHAPE: Shapes? = Shapes.ROUNDED
}
