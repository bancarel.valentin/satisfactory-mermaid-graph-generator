package com.bancarelvalentin.smgg.theme.enums

import com.bancarelvalentin.smgg.theme.themes.partial.DarkerColorsSubPartialTheme
import com.bancarelvalentin.smgg.theme.themes.partial.HorizontalTheme

enum class PartialThemes : ThemesEnum {
    DARKER_COLORS {
        override val clazz = DarkerColorsSubPartialTheme::class.java
    },
    HORIZONTAL {
        override val clazz = HorizontalTheme::class.java
    },
}
