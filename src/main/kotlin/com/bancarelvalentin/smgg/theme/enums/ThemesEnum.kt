package com.bancarelvalentin.smgg.theme.enums

import com.bancarelvalentin.smgg.theme.Theme

interface ThemesEnum {
    val clazz: Class<out Theme>
}
