package com.bancarelvalentin.smgg.theme.enums

import com.bancarelvalentin.smgg.theme.themes.ColorfullTheme
import com.bancarelvalentin.smgg.theme.themes.PastelTheme
import com.bancarelvalentin.smgg.theme.themes.RandomTheme

enum class FullThemes : ThemesEnum {
    RANDOM {
        override val clazz = RandomTheme::class.java
    },
    PASTEL {
        override val clazz = PastelTheme::class.java
    },
    COLORFULL {
        override val clazz = ColorfullTheme::class.java
    },
}
