package com.bancarelvalentin.smgg.theme

import com.bancarelvalentin.smgg.mermaid.MermaidClass
import com.bancarelvalentin.smgg.mermaid.enums.Formats
import com.bancarelvalentin.smgg.mermaid.enums.Links
import com.bancarelvalentin.smgg.mermaid.enums.Orientations
import com.bancarelvalentin.smgg.mermaid.enums.Shapes
import com.bancarelvalentin.smgg.theme.enums.ThemesEnum
import com.bancarelvalentin.smgg.theme.themes.PastelTheme
import java.math.RoundingMode
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

class ThemeHolderManager : Theme() {
    companion object {
        var _theme: Theme = PastelTheme()

        fun set(theme: ThemesEnum) {
            set(theme.clazz.constructors.first().newInstance() as Theme)
        }

        fun set(theme: Theme) {
            _theme = theme
        }

        fun apply(vararg subThemes: ThemesEnum) {
            apply(* subThemes.map { it.clazz.constructors.first().newInstance() as Theme }.toTypedArray())
        }

        fun apply(vararg subThemes: Theme) {
            subThemes.forEach { theme ->
                Theme::class.memberProperties
                    .filterIsInstance<KMutableProperty<*>>()
                    .filter { newProperty -> newProperty.call(theme) != null } // Only NonNull null properties
                    .forEach { newProperty -> // Foreach values to apply => Find matching one in existing theme
                        val existingProp = _theme::class.memberProperties
                            .filterIsInstance<KMutableProperty<*>>()
                            .findLast { subMember -> subMember.name == newProperty.name }!!
                        val existingValue = existingProp.call(_theme)
                        val newValue = newProperty.call(theme)
                        if (existingValue != null && newValue is ThemeUpdatable) {
                            (existingValue as ThemeUpdatable).update(newValue)
                        } else {
                            existingProp.setter.call(_theme, newValue) // Apply new value to current theme
                        }
                    }
            }
        }
    }

    override var ORIENTATION: Orientations? = _theme.ORIENTATION
    override var FORMAT: Formats? = _theme.FORMAT

    override var NO_ROUNDING: Boolean? = _theme.NO_ROUNDING
    override var ROUNDING_DECIMAL_PLACES: Int? = _theme.ROUNDING_DECIMAL_PLACES
    override var ROUNDING_MODE: RoundingMode? = _theme.ROUNDING_MODE

    override var USER_INPUTS_CLASS: MermaidClass? = _theme.USER_INPUTS_CLASS
    override var USER_INPUTS_SHAPE: Shapes? = _theme.USER_INPUTS_SHAPE

    override var EXTRACTORS_CLASS: MermaidClass? = _theme.EXTRACTORS_CLASS
    override var EXTRACTORS_SHAPE: Shapes? = _theme.EXTRACTORS_SHAPE

    override var RAWS_CLASS: MermaidClass? = _theme.RAWS_CLASS
    override var RAWS_SHAPE: Shapes? = _theme.RAWS_SHAPE

    override var TRANSFORMERS_CLASS: MermaidClass? = _theme.TRANSFORMERS_CLASS
    override var TRANSFORMERS_SHAPE: Shapes? = _theme.TRANSFORMERS_SHAPE

    override var CRAFTABLES_CLASS: MermaidClass? = _theme.CRAFTABLES_CLASS
    override var CRAFTABLES_SHAPE: Shapes? = _theme.CRAFTABLES_SHAPE

    override var OUTPUT_CLASS: MermaidClass? = _theme.OUTPUT_CLASS
    override var OUTPUT_SHAPE: Shapes? = _theme.OUTPUT_SHAPE

    override var OUTUTS_LINK_TYPE: Links? = _theme.OUTUTS_LINK_TYPE
    override var INPUTS_LINK_TYPE: Links? = _theme.INPUTS_LINK_TYPE
    override var FINAL_OUTPUT_LINK_TYPE: Links? = _theme.FINAL_OUTPUT_LINK_TYPE

    override var DEFAULT_FILL_COLOR: String? = _theme.DEFAULT_FILL_COLOR
    override var DEFAULT_TEXT_COLOR: String? = _theme.DEFAULT_TEXT_COLOR
    override var DEFAULT_STROKE_COLOR: String? = _theme.DEFAULT_STROKE_COLOR
    override var DEFAULT_STROKE_WIDTH: Int? = _theme.DEFAULT_STROKE_WIDTH
    override var DEFAULT_STROKE_DASHES: Pair<Int, Int>? = _theme.DEFAULT_STROKE_DASHES
}
