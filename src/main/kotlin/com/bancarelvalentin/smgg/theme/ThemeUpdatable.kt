package com.bancarelvalentin.smgg.theme

interface ThemeUpdatable {

    fun update(value: Any)
}
