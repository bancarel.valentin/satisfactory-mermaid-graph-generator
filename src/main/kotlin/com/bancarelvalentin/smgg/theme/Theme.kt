package com.bancarelvalentin.smgg.theme

import com.bancarelvalentin.smgg.mermaid.MermaidClass
import com.bancarelvalentin.smgg.mermaid.enums.Formats
import com.bancarelvalentin.smgg.mermaid.enums.Links
import com.bancarelvalentin.smgg.mermaid.enums.Orientations
import com.bancarelvalentin.smgg.mermaid.enums.Shapes
import java.math.RoundingMode

@Suppress("unused")
abstract class Theme {
    open var ORIENTATION: Orientations? = null
    open var FORMAT: Formats? = null

    open var NO_ROUNDING: Boolean? = false
    open var ROUNDING_DECIMAL_PLACES: Int? = 3
    open var ROUNDING_MODE: RoundingMode? = RoundingMode.DOWN

    open var USER_INPUTS_CLASS: MermaidClass? = null
    open var USER_INPUTS_SHAPE: Shapes? = null

    open var EXTRACTORS_CLASS: MermaidClass? = null
    open var EXTRACTORS_SHAPE: Shapes? = null

    open var RAWS_CLASS: MermaidClass? = null
    open var RAWS_SHAPE: Shapes? = null

    open var TRANSFORMERS_CLASS: MermaidClass? = null
    open var TRANSFORMERS_SHAPE: Shapes? = null

    open var CRAFTABLES_CLASS: MermaidClass? = null
    open var CRAFTABLES_SHAPE: Shapes? = null

    open var OUTPUT_CLASS: MermaidClass? = null
    open var OUTPUT_SHAPE: Shapes? = null

    open var OUTUTS_LINK_TYPE: Links? = null
    open var INPUTS_LINK_TYPE: Links? = null
    open var FINAL_OUTPUT_LINK_TYPE: Links? = null

    val DEFAULT_CLASS_NAME: String = "default"
    open var DEFAULT_FILL_COLOR: String? = null
    open var DEFAULT_TEXT_COLOR: String? = null
    open var DEFAULT_STROKE_COLOR: String? = null
    open var DEFAULT_STROKE_WIDTH: Int? = null
    open var DEFAULT_STROKE_DASHES: Pair<Int, Int>? = null

    @Suppress("LeakingThis")
    val DEFAULT_CLASS: MermaidClass = MermaidClass(
        DEFAULT_CLASS_NAME,
        DEFAULT_FILL_COLOR,
        DEFAULT_TEXT_COLOR,
        DEFAULT_STROKE_COLOR,
        DEFAULT_STROKE_WIDTH,
        DEFAULT_STROKE_DASHES
    )

    protected val DARK = "222222"
    protected val LIGHT = "DDDDDD"
}
