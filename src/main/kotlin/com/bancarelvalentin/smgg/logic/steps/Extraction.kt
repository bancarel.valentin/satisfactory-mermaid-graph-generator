package com.bancarelvalentin.smgg.logic.steps

import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe

class Extraction(override val recipe: ExtractionRecipe, override var overclockValue: Double = 0.0) : Step {

    constructor(recipe: ExtractionRecipe, overclockValue: Int = 0) : this(recipe, overclockValue.toDouble())

    override val mermaidLabel: String
        get() {
            return "${recipe.building.mermaidLabel} PRODUCING '${recipe.label}'"
        }

    override fun toString(): String {
        return "[$mermaidId : $recipe]"
    }
}
