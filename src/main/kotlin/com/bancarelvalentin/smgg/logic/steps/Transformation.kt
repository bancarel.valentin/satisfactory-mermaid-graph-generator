package com.bancarelvalentin.smgg.logic.steps

import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.utils.StringUtils

class Transformation(override val recipe: TransformationRecipe, override var overclockValue: Double = 0.0) : Step {

    constructor(recipe: TransformationRecipe, overclockValue: Int = 0) : this(recipe, overclockValue.toDouble())

    override val mermaidLabel: String
        get() {
            return "[$ocStringValue%] " +
                "${recipe.building.label} USING RECIPE '${recipe.label}'  |  " +
                "In: ${recipe.inputs.map { it.perMin }.joinToString(" - ")}  |  " +
                "Out: ${recipe.outputs.map { it.perMin }.joinToString(" - ")}"
        }

    private val ocStringValue get() = StringUtils.formatDecimalNumber(overclockValue * 100)

    override fun toString(): String {
        return "[$mermaidId @ $ocStringValue% : $recipe]"
    }
}
