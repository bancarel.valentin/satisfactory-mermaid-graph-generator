package com.bancarelvalentin.smgg.logic.steps

import com.bancarelvalentin.smgg.logic.enums.recipe.UserManualInput

class StorageInput(override val recipe: UserManualInput, override var overclockValue: Double = 0.0) : Step {

    override val mermaidLabel: String
        get() {
            return "${recipe.building.label} giving ${recipe.outputs[0].item.label}"
        }

    override fun toString(): String {
        return "[$mermaidId : $recipe]"
    }
}
