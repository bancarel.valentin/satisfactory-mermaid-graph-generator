package com.bancarelvalentin.smgg.logic.steps

import com.bancarelvalentin.smgg.logic.enums.recipe.Recipe
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.mermaid.MermaidBuilder
import com.bancarelvalentin.smgg.mermaid.MermaidDefinition
import com.bancarelvalentin.smgg.mermaid.MermaidLine
import com.bancarelvalentin.smgg.mermaid.Mermaidable
import com.bancarelvalentin.smgg.theme.ThemeHolderManager
import com.bancarelvalentin.smgg.utils.StringUtils

interface Step : Mermaidable {

    val recipe: Recipe
    var overclockValue: Double

    override val mermaidId: String
        get() {
            return "M__${recipe.building.mermaidId}_${recipe.id}"
        }

    override val mermaidDefinition: MermaidDefinition
        get() {
            return MermaidDefinition(
                mermaidId,
                mermaidLabel,
                recipe.building.mermaidDefinition.shape,
                recipe.building.mermaidDefinition.className
            )
        }

    fun oc(value: Double) {
        this.overclockValue += value
    }

    fun getOutput(rate: Rate): Double {
        return rate.perMin * overclockValue
    }

    fun toMermaid(mermaidBuilder: MermaidBuilder) {

        mermaidBuilder.comment(this.recipe.label)
            .add(
                *recipe.inputs.map {
                    MermaidLine(
                        it.item.id,
                        mermaidId,
                        ThemeHolderManager().INPUTS_LINK_TYPE,
                        transfromOutput(it)
                    )
                }.toTypedArray()
            )
            .breakline()
            .add(
                *recipe.outputs.map {
                    MermaidLine(
                        mermaidId,
                        it.item.id,
                        ThemeHolderManager().OUTUTS_LINK_TYPE,
                        transfromOutput(it)
                    )
                }.toTypedArray()
            )
            .breakline()
            .comment("-------------------------------")
            .breakline()
    }

    private fun transfromOutput(rate: Rate): String {
        return StringUtils.formatDecimalNumber(getOutput(rate))
    }
}
