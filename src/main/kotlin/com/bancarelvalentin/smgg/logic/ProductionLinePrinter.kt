package com.bancarelvalentin.smgg.logic

import com.bancarelvalentin.smgg.Constants
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.mermaid.MermaidBuilder
import com.bancarelvalentin.smgg.mermaid.MermaidDefinition
import com.bancarelvalentin.smgg.mermaid.MermaidLine
import com.bancarelvalentin.smgg.theme.ThemeHolderManager
import com.bancarelvalentin.smgg.utils.StringUtils
import java.io.PrintStream
import java.nio.file.Files
import java.nio.file.Path

class ProductionLinePrinter(@Suppress("CanBeParameter") val productionLines: Array<ProductionLine>, vararg titles: String) {

    constructor(productionLine: ProductionLine, vararg titles: String) : this(arrayOf(productionLine), *titles)

    private var outputs: ArrayList<String> = ArrayList()

    init {
        var i = -1
        for (graph in productionLines) {
            i++
            val title = try {
                titles[i]
            } catch (e: ArrayIndexOutOfBoundsException) {
                null
            }
            val mermaidBuilder = MermaidBuilder(ThemeHolderManager().FORMAT, title)
            val metaOut = MermaidDefinition(
                "FACTORY_OUTPUT",
                "Factory output",
                ThemeHolderManager().OUTPUT_SHAPE,
                ThemeHolderManager().OUTPUT_CLASS?.name
            )
            mermaidBuilder.init(ThemeHolderManager().ORIENTATION)
                .comment(StringUtils.formatDate())
                .breakline()
                .comment("generic style")
                .addStyle(ThemeHolderManager().USER_INPUTS_CLASS)
                .addStyle(ThemeHolderManager().DEFAULT_CLASS)
                .addStyle(ThemeHolderManager().EXTRACTORS_CLASS)
                .addStyle(ThemeHolderManager().RAWS_CLASS)
                .addStyle(ThemeHolderManager().TRANSFORMERS_CLASS)
                .addStyle(ThemeHolderManager().CRAFTABLES_CLASS)
                .addStyle(ThemeHolderManager().OUTPUT_CLASS)
                .breakline()
                .breakline()
                .define(metaOut)
                .breakline()
                .comment("Manual inputs definition")
                .define(* graph.userInputs.map { it.value.mermaidDefinition }.toTypedArray())
                .breakline()
                .comment("Extracors definition")
                .define(* graph.extractions.map { it.value.mermaidDefinition }.toTypedArray())
                .breakline()
                .comment("Raw resources definition")
                .define(* graph.rawResources.map { it.value.mermaidDefinition }.toTypedArray())
                .breakline()
                .comment("All machine groups")
                .define(* graph.transformations.map { it.value.mermaidDefinition }.toTypedArray())
                .breakline()
                .comment("Intermediate items used in process")
                .define(* graph.intermediateItems.map { it.value.mermaidDefinition }.toTypedArray())
                .breakline()

            mermaidBuilder.comment("User inputs to item")
            graph.userInputs.forEach { it.value.toMermaid(mermaidBuilder) }

            mermaidBuilder.comment("Extraction")
            graph.extractions.forEach { it.value.toMermaid(mermaidBuilder) }

            mermaidBuilder.comment("Transformation")
            graph.transformations.forEach { it.value.toMermaid(mermaidBuilder) }

            mermaidBuilder.comment("Output")
            mermaidBuilder.add(
                *graph.outputs.map {
                    MermaidLine(
                        it.mermaidId,
                        metaOut.id,
                        ThemeHolderManager().FINAL_OUTPUT_LINK_TYPE,
                        StringUtils.formatDecimalNumber(it.perMin)
                    )
                }.toTypedArray()
            )

            outputs.add(mermaidBuilder.build())
        }
    }

    @Deprecated("Used for sandbox testing only")
    fun sandboxPrint(): ProductionLinePrinter {
        for ((i, valu) in printAll().withIndex()) {
            val pl = productionLines[i]
            val sandboxTitle = StringUtils.fsAllowedOnly("Sandbox (${pl.outputs.joinToString(" - ") { "${it.item.label} @ ${it.perMin}min" }})")
            val dir = Constants.DEFAULT_OUT_DIR.resolve("sandbox").resolve(sandboxTitle)
            dir.toFile().mkdirs()

            val pathMd = dir.resolve("$sandboxTitle.md")
            Files.write(pathMd, valu.toByteArray())

            val pathRaw = dir.resolve("$sandboxTitle.txt")
            Files.write(pathRaw, pl.toString(true).toByteArray())

            println("Printed to folder ${dir.toAbsolutePath()}")
        }
        return this
    }

    fun print(file: Path): ProductionLinePrinter {
        Files.write(file, print().toByteArray())
        println("Printed to ${file.toAbsolutePath()}")
        return this
    }

    @Suppress("unused")
    fun print(stream: PrintStream): ProductionLinePrinter {
        stream.println(print())
        return this
    }

    fun print(): String {
        return outputs.joinToString("\n\n")
    }

    fun printAll(): ArrayList<String> {
        return outputs
    }
}
