package com.bancarelvalentin.smgg.logic

import com.bancarelvalentin.smgg.AutoRecipeLoopException
import com.bancarelvalentin.smgg.ComputeAlreadyDoneException
import com.bancarelvalentin.smgg.UnknownException
import com.bancarelvalentin.smgg.UnknownItemType
import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.Recipe
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.UserManualInput
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.steps.Extraction
import com.bancarelvalentin.smgg.logic.steps.StorageInput
import com.bancarelvalentin.smgg.logic.steps.Transformation

class ProductionLineGenerator {

    val wishes = java.util.ArrayList<Rate>()
    private val transformRecipes = ArrayList<TransformationRecipe>()
    private val extractRecipes = ArrayList<ExtractionRecipe>()
    private lateinit var productionLine: ProductionLine

    fun addOutput(vararg outputs: Rate): ProductionLineGenerator {
        this.wishes.addAll(outputs)
        return this
    }

    fun preferTransformationRecipe(vararg recipe: TransformationRecipe): ProductionLineGenerator {
        transformRecipes.addAll(recipe)
        return this
    }

    fun preferExtractionRecipe(vararg recipe: ExtractionRecipe): ProductionLineGenerator {
        extractRecipes.addAll(recipe)
        return this
    }

    fun generate(): ProductionLine {
        return try {
            if (this::productionLine.isInitialized) throw ComputeAlreadyDoneException()

            // Setup production line outputs for auto generation
            productionLine = ProductionLine()
            this.productionLine.outputs.addAll(wishes)

            // Do maths
            productionLine.outputs.forEach { addOrEditStep(it) }

            // Handle byproduct
            productionLine.transformations
                .forEach { trans ->
                    trans.value.recipe.outputs
                        .filter { it.item != trans.key }
                        .forEach { handleByProduct(it, trans.value) }
                }

            println("Generated. $productionLine")
            this.productionLine
        } catch (e: StackOverflowError) {
            throw AutoRecipeLoopException(e)
        } catch (e: Exception) {
            throw UnknownException(e)
        }
    }

    private fun addOrEditStep(
        rate: Rate,
        parentOcFactor: Double = 1.0,
        index: Int = 0,
        chain: Array<Recipe> = emptyArray(),
    ) {
        println("step for $rate")

        var recipe: Recipe?
        val ocFactor: Double
        val step = if (rate.item is CraftableItem) {
            recipe = findTransformationRecipe(rate.item)
            productionLine.transformations.computeIfAbsent(rate.item) { Transformation(recipe as TransformationRecipe, 0) }
            productionLine.transformations[rate.item]!!
        } else if (rate.item is RawItem) {
            recipe = findExtractionRecipe(rate.item)
            if (recipe != null) {
                productionLine.extractions.computeIfAbsent(rate.item) { Extraction(recipe as ExtractionRecipe, 0) }
                productionLine.extractions[rate.item]!!
            } else {
                recipe = UserManualInput(rate)
                productionLine.userInputs.computeIfAbsent(rate.item) { StorageInput(recipe) }
                productionLine.userInputs[rate.item]!!
            }
        } else {
            throw UnknownItemType(rate.item::class.java)
        }

        val baseProducedRate = recipe.getOutput(rate.item).perMin
        val neededRate = rate.perMin * parentOcFactor
        ocFactor = neededRate / baseProducedRate
        step.oc(ocFactor)
        println("\trecipe: ${step.recipe}")
        println("\toc: $ocFactor")

        // outputs
        recipe.outputs
            .forEach { oRate ->
                when (oRate.item) {
                    is CraftableItem -> {
                        productionLine.intermediateItems.computeIfAbsent(oRate.item) { Rate(it, 0) }
                        productionLine.intermediateItems[oRate.item]!!.add(oRate.perMin * ocFactor)
                    }
                    is RawItem -> {
                        productionLine.rawResources.computeIfAbsent(oRate.item) { Rate(it, 0) }
                        productionLine.rawResources[oRate.item]!!.add(oRate.perMin * ocFactor)
                    }
                }
            }

        // Trigger childs
        recipe.inputs.copyOf().forEach { addOrEditStep(it, ocFactor, index + 1, arrayOf(recipe, *chain.copyOf())) }
    }

    private fun handleByProduct(byProduct: Rate, from: Transformation) {
        when (byProduct.item) {
            is RawItem -> {
                productionLine.extractions.values
                    .forEach { extraction ->
                        extraction.recipe.outputs
                            .filter { rate -> rate.item == byProduct.item }
                            .forEach { rate ->
                                extraction.oc((byProduct.perMin * from.overclockValue) / rate.perMin * -1)
                            }
                    }
                // productionLine.rawResources.values.forEach { rate->
                //     rate.add(byProduct.perMin * from.overclockValue*-1)
                // }
            }
            is CraftableItem -> {
                productionLine.transformations.values
                    .filter { it.mermaidId != from.mermaidId }
                    .forEach { transformation ->
                        transformation.recipe.outputs
                            .filter { rate -> rate.item == byProduct.item }
                            .forEach { rate ->
                                val value = (byProduct.perMin * from.overclockValue) / rate.perMin * -1
                                transformation.oc(value)
                            }
                    }
                // productionLine.intermediateItems.values.forEach { rate->
                //     rate.add(byProduct.perMin * from.overclockValue*-1)
                // }
            }
            else -> {
                throw UnknownItemType(byProduct.item::class.java)
            }
        }
    }

    private fun findTransformationRecipe(item: CraftableItem): TransformationRecipe {
        return transformRecipes.find { it.outputs.find { rate -> rate.item == item } != null }
            ?: TransformationRecipe.getAny(item)
    }

    private fun findExtractionRecipe(item: RawItem): ExtractionRecipe? {
        return extractRecipes.find { it.outputs.find { rate -> rate.item == item } != null }
            ?: ExtractionRecipe.getAny(item)
    }
}
