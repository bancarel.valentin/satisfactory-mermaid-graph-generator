package com.bancarelvalentin.smgg.logic.model

import com.bancarelvalentin.smgg.logic.enums.item.Item

class RateOfOne(item: Item) : Rate(item, 1)
