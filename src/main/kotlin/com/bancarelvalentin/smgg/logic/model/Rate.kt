package com.bancarelvalentin.smgg.logic.model

import com.bancarelvalentin.smgg.TryingToAddToDifferentRatesException
import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.Item
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.mermaid.MermaidDefinition
import com.bancarelvalentin.smgg.mermaid.Mermaidable
import com.bancarelvalentin.smgg.theme.ThemeHolderManager

open class Rate(val item: Item, var perMin: Double) : Mermaidable {

    constructor(item: Item, perMin: Int) : this(item, perMin.toDouble())

    fun add(rate: Rate) {
        if (!this.item.isSameTypeAs(rate.item)) throw TryingToAddToDifferentRatesException()
        add(rate.perMin)
    }

    fun add(rate: Double) {
        perMin += rate
    }

    override val mermaidId: String get() = item.id
    override val mermaidLabel: String get() = "${item.label} (${this.perMin}/min)"

    override val mermaidDefinition: MermaidDefinition
        get() {
            return when (item) {
                is RawItem -> MermaidDefinition(
                    mermaidId,
                    mermaidLabel,
                    ThemeHolderManager().RAWS_SHAPE,
                    ThemeHolderManager().RAWS_CLASS?.name
                )

                is CraftableItem -> MermaidDefinition(
                    mermaidId,
                    mermaidLabel,
                    ThemeHolderManager().CRAFTABLES_SHAPE,
                    ThemeHolderManager().CRAFTABLES_CLASS?.name
                )
                else -> throw RuntimeException()
            }
        }
    override fun toString(): String {
        return "[$mermaidId - $perMin/min]"
    }
}
