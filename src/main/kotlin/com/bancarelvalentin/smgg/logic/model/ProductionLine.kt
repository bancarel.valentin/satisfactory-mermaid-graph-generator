package com.bancarelvalentin.smgg.logic.model

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.Item
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.steps.Extraction
import com.bancarelvalentin.smgg.logic.steps.StorageInput
import com.bancarelvalentin.smgg.logic.steps.Transformation
import com.bancarelvalentin.smgg.utils.StringUtils

class ProductionLine {

    val outputs = ArrayList<Rate>()
    val extractions = HashMap<RawItem, Extraction>()
    val userInputs = HashMap<Item, StorageInput>()
    val rawResources = HashMap<RawItem, Rate>()
    val transformations = HashMap<CraftableItem, Transformation>()
    val intermediateItems = HashMap<CraftableItem, Rate>()

    val nbNodes
        get() = transformations.size + extractions.size + rawResources.size + intermediateItems.size

    override fun toString(): String {
        return toString(false)
    }

    fun toString(pretty: Boolean): String {
        val string = "[Extraction: [${extractions.map { it.value.toString() }.joinToString(" ")}]" +
            " - User inputs: [${userInputs.map { it.value.toString() }.joinToString(" ")}]" +
            " - Raw items: [${rawResources.map { it.value.toString() }.joinToString(" ")}]" +
            " - Transformation: [${transformations.map { it.value.toString() }.joinToString(" ")}]" +
            " - Intermediate items: [${intermediateItems.map { it.value.toString() }.joinToString(" ")}]" +
            " - Outputs: [${outputs.map { it.toString() }.joinToString(" ")}]]"

        return if (pretty) {
            var ret = ""
            var delta = 0
            for (char in string) {
                when (char) {
                    '-' -> {
                        ret += "\n" + StringUtils.tabs(delta)
                    }
                    '[' -> {
                        delta++
                        ret += char + "\n" + StringUtils.tabs(delta)
                    }
                    ']' -> {
                        delta--
                        ret += "\n" + StringUtils.tabs(delta) + char
                    }
                    else -> {
                        ret += char
                    }
                }
            }
            ret
        } else {
            string
        }
    }
}
