package com.bancarelvalentin.smgg.logic.enums.recipe

import com.bancarelvalentin.smgg.logic.enums.building.Building
import com.bancarelvalentin.smgg.logic.enums.building.StorageContainer
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.model.RateOfOne

class UserManualInput(rate: Rate) : Recipe {
    override val building: Building = StorageContainer()
    override val isAlternative: Boolean = false
    override val inputs: Array<Rate> = emptyArray()
    override val outputs: Array<Rate> = arrayOf(RateOfOne(rate.item))
    override val label: String = "Manual input for ${rate.item.label}"
}
