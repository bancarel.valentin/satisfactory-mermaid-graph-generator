package com.bancarelvalentin.smgg.logic.enums.item

@Suppress("unused")
enum class RawItem(
    override val label: String,
    override val hasNoRecipe: Boolean = false,
    override val itemState: ItemState = ItemState.SOLID
) : Item {
    ALIEN_CARAPACE("Alien Carapace", true),
    ALIEN_ORGANS("Alien Organs", true),
    BAUXITE("Bauxite"),
    BLUE_POWER_SLUG("Blue power slug", true),
    CATERIUM_ORE("Caterium Ore"),
    COAL("Coal"),
    COPPER_ORE("Copper Ore"),
    CRUDE_OIL("Crude Oil", itemState = ItemState.FUILD),
    FLOWER_PETALS("Flower Petals", true),
    GREEN_POWER_SLUG("Green power slug", true),
    IRON_ORE("Iron Ore"),
    LEAVES("Leaves", true),
    LIMESTONE("Limestone"),
    MYCELIA("Mycelia", true),
    NITROGEN_GAS("Nitrogen Gas", itemState = ItemState.FUILD),
    RAW_QUARTZ("Raw Quartz"),
    SAM_ORE("S.A.M Ore"),
    SULFUR("Sulfur"),
    URANIUM("Uranium"),
    WATER("Water", itemState = ItemState.FUILD),
    WOOD("Wood", true),
    YELLOW_POWER_SLUG("Yellow power slug", true),
}
