package com.bancarelvalentin.smgg.logic.enums.recipe

import com.bancarelvalentin.smgg.AltRecipeNotFoundException
import com.bancarelvalentin.smgg.StandardRecipeNotFoundException
import com.bancarelvalentin.smgg.logic.enums.building.TransformerBuilding
import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.model.Rate

@Suppress("unused")
enum class TransformationRecipe(
    override val building: TransformerBuilding,
    override val label: String,
    override val isAlternative: Boolean,
    override val inputs: Array<Rate>,
    override val outputs: Array<Rate>,
) : Recipe {
    ADAPTIVE_CONTROL_UNIT(
        TransformerBuilding.MANUFACTURER,
        "[S] Adaptive Control Unit ",
        false,
        arrayOf(Rate(CraftableItem.AUTOMATED_WIRING, 7.5), Rate(CraftableItem.CIRCUIT_BOARD, 5), Rate(CraftableItem.HEAVY_MODULAR_FRAME, 1), Rate(CraftableItem.COMPUTER, 1)),
        arrayOf(Rate(CraftableItem.ADAPTIVE_CONTROL_UNIT, 1))
    ),
    AI_LIMITER(
        TransformerBuilding.ASSEMBLER,
        "[S] AI Limiter ",
        false,
        arrayOf(Rate(CraftableItem.COPPER_SHEET, 25), Rate(CraftableItem.QUICKWIRE, 100)),
        arrayOf(Rate(CraftableItem.AI_LIMITER, 5))
    ),
    ALCLAD_ALUMINUM_SHEET(
        TransformerBuilding.ASSEMBLER,
        "[S] Alclad Aluminum Sheet ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINUM_INGOT, 30), Rate(CraftableItem.COPPER_INGOT, 10)),
        arrayOf(Rate(CraftableItem.ALCLAD_ALUMINUM_SHEET, 30))
    ),
    ALUMINA_SOLUTION(
        TransformerBuilding.REFINERY,
        "[S] Alumina Solution ",
        false,
        arrayOf(Rate(RawItem.BAUXITE, 120), Rate(RawItem.WATER, 180)),
        arrayOf(Rate(CraftableItem.ALUMINA_SOLUTION, 120), Rate(CraftableItem.SILICA, 50))
    ),
    ALUMINA_SOLUTION_ALT_SLOPPY_ALUMINA(
        TransformerBuilding.REFINERY,
        "[A] Sloppy Alumina",
        true,
        arrayOf(Rate(RawItem.BAUXITE, 200), Rate(RawItem.WATER, 200)),
        arrayOf(Rate(CraftableItem.ALUMINA_SOLUTION, 240))
    ),
    ALUMINA_SOLUTION_ALT_UNPACKAGE_ALUMINA_SOLUTION(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Alumina Solution",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_ALUMINA_SOLUTION, 120)),
        arrayOf(Rate(CraftableItem.ALUMINA_SOLUTION, 120), Rate(CraftableItem.EMPTY_CANISTER, 120))
    ),
    ALUMINUM_CASING(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Aluminum Casing ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINUM_INGOT, 90)),
        arrayOf(Rate(CraftableItem.ALUMINUM_CASING, 60))
    ),
    ALUMINUM_CASING_ALT_ALCLAD_CASING(
        TransformerBuilding.ASSEMBLER,
        "[A] Alclad Casing",
        true,
        arrayOf(Rate(CraftableItem.ALUMINUM_INGOT, 150), Rate(CraftableItem.COPPER_INGOT, 75)),
        arrayOf(Rate(CraftableItem.ALUMINUM_CASING, 112.5))
    ),
    ALUMINUM_INGOT(
        TransformerBuilding.FOUNDRY,
        "[S] Aluminum Ingot ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINUM_SCRAP, 90), Rate(CraftableItem.SILICA, 75)),
        arrayOf(Rate(CraftableItem.ALUMINUM_INGOT, 60))
    ),
    ALUMINUM_INGOT_ALT_PURE_ALUMINUM_INGOT(
        TransformerBuilding.SMELTER,
        "[A] Pure Aluminum Ingot",
        true,
        arrayOf(Rate(CraftableItem.ALUMINUM_SCRAP, 60)),
        arrayOf(Rate(CraftableItem.ALUMINUM_INGOT, 30))
    ),
    ALUMINUM_SCRAP(
        TransformerBuilding.REFINERY,
        "[S] Aluminum Scrap ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINA_SOLUTION, 240), Rate(RawItem.COAL, 120)),
        arrayOf(Rate(CraftableItem.ALUMINUM_SCRAP, 360), Rate(RawItem.WATER, 120))
    ),
    ALUMINUM_SCRAP_ALT_ELECTRODE_ALUMINUM_SCRAP(
        TransformerBuilding.REFINERY,
        "[A] Electrode - Aluminum Scrap",
        true,
        arrayOf(Rate(CraftableItem.ALUMINA_SOLUTION, 180), Rate(CraftableItem.PETROLEUM_COKE, 60)),
        arrayOf(Rate(CraftableItem.ALUMINUM_SCRAP, 300), Rate(RawItem.WATER, 105))
    ),
    ALUMINUM_SCRAP_ALT_INSTANT_SCRAP(
        TransformerBuilding.BLENDER,
        "[A] Instant Scrap",
        true,
        arrayOf(Rate(RawItem.BAUXITE, 150), Rate(RawItem.COAL, 100), Rate(CraftableItem.SULFURIC_ACID, 50), Rate(RawItem.WATER, 60)),
        arrayOf(Rate(CraftableItem.ALUMINUM_SCRAP, 300), Rate(RawItem.WATER, 50))
    ),
    ASSEMBLY_DIRECTOR_SYSTEM(
        TransformerBuilding.ASSEMBLER,
        "[S] Assembly Director System ",
        false,
        arrayOf(Rate(CraftableItem.ADAPTIVE_CONTROL_UNIT, 1.5), Rate(CraftableItem.SUPERCOMPUTER, 0.75)),
        arrayOf(Rate(CraftableItem.ASSEMBLY_DIRECTOR_SYSTEM, 0.8))
    ),
    AUTOMATED_WIRING(
        TransformerBuilding.ASSEMBLER,
        "[S] Automated Wiring ",
        false,
        arrayOf(Rate(CraftableItem.STATOR, 2.5), Rate(CraftableItem.CABLE, 50)),
        arrayOf(Rate(CraftableItem.AUTOMATED_WIRING, 2.5))
    ),
    AUTOMATED_WIRING_ALT_AUTOMATED_SPEED_WIRING(
        TransformerBuilding.MANUFACTURER,
        "[A] Automated Speed Wiring",
        true,
        arrayOf(Rate(CraftableItem.STATOR, 3.75), Rate(CraftableItem.WIRE, 75), Rate(CraftableItem.HIGH_SPEED_CONNECTOR, 1.875)),
        arrayOf(Rate(CraftableItem.AUTOMATED_WIRING, 7.5))
    ),
    BATTERY(
        TransformerBuilding.BLENDER,
        "[S] Battery ",
        false,
        arrayOf(Rate(CraftableItem.SULFURIC_ACID, 50), Rate(CraftableItem.ALUMINA_SOLUTION, 40), Rate(CraftableItem.ALUMINUM_CASING, 20)),
        arrayOf(Rate(CraftableItem.BATTERY, 20), Rate(RawItem.WATER, 30))
    ),
    BATTERY_ALT_CLASSIC_BATTERY(
        TransformerBuilding.MANUFACTURER,
        "[A] Classic Battery",
        true,
        arrayOf(Rate(RawItem.SULFUR, 45), Rate(CraftableItem.ALCLAD_ALUMINUM_SHEET, 52.5), Rate(CraftableItem.PLASTIC, 60), Rate(CraftableItem.WIRE, 90)),
        arrayOf(Rate(CraftableItem.BATTERY, 30))
    ),
    BEACON(
        TransformerBuilding.MANUFACTURER,
        "[S] Beacon ",
        false,
        arrayOf(Rate(CraftableItem.IRON_PLATE, 22.5), Rate(CraftableItem.IRON_ROD, 7.5), Rate(CraftableItem.WIRE, 112.5), Rate(CraftableItem.CABLE, 15)),
        arrayOf(Rate(CraftableItem.BEACON, 7.5))
    ),
    BEACON_ALT_CRYSTAL_BEACON(
        TransformerBuilding.MANUFACTURER,
        "[A] Crystal Beacon",
        true,
        arrayOf(Rate(CraftableItem.STEEL_BEAM, 2), Rate(CraftableItem.STEEL_PIPE, 8), Rate(CraftableItem.CRYSTAL_OSCILLATOR, 0.5)),
        arrayOf(Rate(CraftableItem.BEACON, 10))
    ),
    BIOMASS_ALT_BIOMASS_ALIEN_CARAPACE(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Biomass (Alien Carapace)",
        true,
        arrayOf(Rate(RawItem.ALIEN_CARAPACE, 15)),
        arrayOf(Rate(CraftableItem.BIOMASS, 1500))
    ),
    BIOMASS_ALT_BIOMASS_ALIEN_ORGANS(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Biomass (Alien Organs)",
        true,
        arrayOf(Rate(RawItem.ALIEN_ORGANS, 7.5)),
        arrayOf(Rate(CraftableItem.BIOMASS, 1500))
    ),
    BIOMASS_ALT_BIOMASS_LEAVES(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Biomass (Leaves)",
        true,
        arrayOf(Rate(RawItem.LEAVES, 120)),
        arrayOf(Rate(CraftableItem.BIOMASS, 60))
    ),
    BIOMASS_ALT_BIOMASS_MYCELIA(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Biomass (Mycelia)",
        true,
        arrayOf(Rate(RawItem.MYCELIA, 150)),
        arrayOf(Rate(CraftableItem.BIOMASS, 150))
    ),
    BIOMASS_ALT_BIOMASS_WOOD(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Biomass (Wood)",
        true,
        arrayOf(Rate(RawItem.WOOD, 60)),
        arrayOf(Rate(CraftableItem.BIOMASS, 300))
    ),
    BLACK_POWDER(
        TransformerBuilding.ASSEMBLER,
        "[S] Black Powder ",
        false,
        arrayOf(Rate(RawItem.COAL, 7.5), Rate(RawItem.SULFUR, 15)),
        arrayOf(Rate(CraftableItem.BLACK_POWDER, 7.5))
    ),
    BLACK_POWDER_ALT_FINE_BLACK_POWDER(
        TransformerBuilding.ASSEMBLER,
        "[A] Fine Black Powder",
        true,
        arrayOf(Rate(CraftableItem.COMPACTED_COAL, 3.75), Rate(RawItem.SULFUR, 7.5)),
        arrayOf(Rate(CraftableItem.BLACK_POWDER, 15))
    ),
    CABLE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Cable ",
        false,
        arrayOf(Rate(CraftableItem.WIRE, 60)),
        arrayOf(Rate(CraftableItem.CABLE, 30))
    ),
    CABLE_ALT_COATED_CABLE(
        TransformerBuilding.REFINERY,
        "[A] Coated Cable",
        true,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 15), Rate(CraftableItem.WIRE, 37.5)),
        arrayOf(Rate(CraftableItem.CABLE, 67.5))
    ),
    CABLE_ALT_INSULATED_CABLE(
        TransformerBuilding.ASSEMBLER,
        "[A] Insulated Cable",
        true,
        arrayOf(Rate(CraftableItem.WIRE, 45), Rate(CraftableItem.RUBBER, 30)),
        arrayOf(Rate(CraftableItem.CABLE, 100))
    ),
    CABLE_ALT_QUICKWIRE_CABLE(
        TransformerBuilding.ASSEMBLER,
        "[A] Quickwire Cable",
        true,
        arrayOf(Rate(CraftableItem.QUICKWIRE, 7.5), Rate(CraftableItem.RUBBER, 5)),
        arrayOf(Rate(CraftableItem.CABLE, 27.5))
    ),
    CATERIUM_INGOT(
        TransformerBuilding.SMELTER,
        "[S] Caterium Ingot ",
        false,
        arrayOf(Rate(RawItem.CATERIUM_ORE, 45)),
        arrayOf(Rate(CraftableItem.CATERIUM_INGOT, 15))
    ),
    CATERIUM_INGOT_ALT_PURE_CATERIUM_INGOT(
        TransformerBuilding.REFINERY,
        "[A] Pure Caterium Ingot",
        true,
        arrayOf(Rate(RawItem.CATERIUM_ORE, 24), Rate(RawItem.WATER, 24)),
        arrayOf(Rate(CraftableItem.CATERIUM_INGOT, 12))
    ),
    CIRCUIT_BOARD(
        TransformerBuilding.ASSEMBLER,
        "[S] Circuit Board ",
        false,
        arrayOf(Rate(CraftableItem.COPPER_SHEET, 15), Rate(CraftableItem.PLASTIC, 30)),
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 7.5))
    ),
    CIRCUIT_BOARD_ALT_CATERIUM_CIRCUIT_BOARD(
        TransformerBuilding.ASSEMBLER,
        "[A] Caterium Circuit Board",
        true,
        arrayOf(Rate(CraftableItem.QUICKWIRE, 37.5), Rate(CraftableItem.PLASTIC, 12.5)),
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 8.8))
    ),
    CIRCUIT_BOARD_ALT_ELECTRODE_CIRCUIT_BOARD(
        TransformerBuilding.ASSEMBLER,
        "[A] Electrode Circuit Board",
        true,
        arrayOf(Rate(CraftableItem.RUBBER, 30), Rate(CraftableItem.PETROLEUM_COKE, 45)),
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 5))
    ),
    CIRCUIT_BOARD_ALT_SILICON_CIRCUIT_BOARD(
        TransformerBuilding.ASSEMBLER,
        "[A] Silicon Circuit Board",
        true,
        arrayOf(Rate(CraftableItem.COPPER_SHEET, 27.5), Rate(CraftableItem.SILICA, 27.5)),
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 12.5))
    ),
    COAL_ALT_BIOCOAL(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Biocoal",
        true,
        arrayOf(Rate(CraftableItem.BIOMASS, 37.5)),
        arrayOf(Rate(RawItem.COAL, 45))
    ),
    COAL_ALT_CHARCOAL(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Charcoal",
        true,
        arrayOf(Rate(RawItem.WOOD, 15)),
        arrayOf(Rate(RawItem.COAL, 150))
    ),
    COLOR_CARTRIDGE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Color Cartridge ",
        false,
        arrayOf(Rate(RawItem.FLOWER_PETALS, 37.5)),
        arrayOf(Rate(CraftableItem.COLOR_CARTRIDGE, 75))
    ),
    COMPACTED_COAL_ALT_COMPACTED_COAL_STANDARD(
        TransformerBuilding.ASSEMBLER,
        "[A] Compacted Coal ",
        true,
        arrayOf(Rate(RawItem.COAL, 25), Rate(RawItem.SULFUR, 25)),
        arrayOf(Rate(CraftableItem.COMPACTED_COAL, 25))
    ),
    COMPUTER(
        TransformerBuilding.MANUFACTURER,
        "[S] Computer ",
        false,
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 25), Rate(CraftableItem.CABLE, 22.5), Rate(CraftableItem.PLASTIC, 45), Rate(CraftableItem.SCREW, 130)),
        arrayOf(Rate(CraftableItem.COMPUTER, 2.5))
    ),
    COMPUTER_ALT_CATERIUM_COMPUTER(
        TransformerBuilding.MANUFACTURER,
        "[A] Caterium Computer",
        true,
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 26.25), Rate(CraftableItem.QUICKWIRE, 105), Rate(CraftableItem.RUBBER, 45)),
        arrayOf(Rate(CraftableItem.COMPUTER, 3.8))
    ),
    COMPUTER_ALT_CRYSTAL_COMPUTER(
        TransformerBuilding.ASSEMBLER,
        "[A] Crystal Computer",
        true,
        arrayOf(Rate(CraftableItem.CIRCUIT_BOARD, 7.5), Rate(CraftableItem.CRYSTAL_OSCILLATOR, 2.8125)),
        arrayOf(Rate(CraftableItem.COMPUTER, 2.8))
    ),
    CONCRETE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Concrete ",
        false,
        arrayOf(Rate(RawItem.LIMESTONE, 45)),
        arrayOf(Rate(CraftableItem.CONCRETE, 15))
    ),
    CONCRETE_ALT_FINE_CONCRETE(
        TransformerBuilding.ASSEMBLER,
        "[A] Fine Concrete",
        true,
        arrayOf(Rate(CraftableItem.SILICA, 7.5), Rate(RawItem.LIMESTONE, 30)),
        arrayOf(Rate(CraftableItem.CONCRETE, 25))
    ),
    CONCRETE_ALT_RUBBER_CONCRETE(
        TransformerBuilding.ASSEMBLER,
        "[A] Rubber Concrete",
        true,
        arrayOf(Rate(CraftableItem.RUBBER, 10), Rate(RawItem.LIMESTONE, 50)),
        arrayOf(Rate(CraftableItem.CONCRETE, 45))
    ),
    CONCRETE_ALT_WET_CONCRETE(
        TransformerBuilding.REFINERY,
        "[A] Wet Concrete",
        true,
        arrayOf(Rate(RawItem.LIMESTONE, 120), Rate(RawItem.WATER, 100)),
        arrayOf(Rate(CraftableItem.CONCRETE, 80))
    ),
    COOLING_SYSTEM(
        TransformerBuilding.BLENDER,
        "[S] Cooling System ",
        false,
        arrayOf(Rate(CraftableItem.HEAT_SINK, 12), Rate(CraftableItem.RUBBER, 12), Rate(RawItem.WATER, 30), Rate(RawItem.NITROGEN_GAS, 150)),
        arrayOf(Rate(CraftableItem.COOLING_SYSTEM, 6))
    ),
    COOLING_SYSTEM_ALT_COOLING_DEVICE(
        TransformerBuilding.BLENDER,
        "[A] Cooling Device",
        true,
        arrayOf(Rate(CraftableItem.HEAT_SINK, 9.375), Rate(CraftableItem.MOTOR, 1.875), Rate(RawItem.NITROGEN_GAS, 45)),
        arrayOf(Rate(CraftableItem.COOLING_SYSTEM, 3.8))
    ),
    COPPER_INGOT(
        TransformerBuilding.SMELTER,
        "[S] Copper Ingot ",
        false,
        arrayOf(Rate(RawItem.COPPER_ORE, 30)),
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 30))
    ),
    COPPER_INGOT_ALT_COPPER_ALLOY_INGOT(
        TransformerBuilding.FOUNDRY,
        "[A] Copper Alloy Ingot",
        true,
        arrayOf(Rate(RawItem.COPPER_ORE, 50), Rate(RawItem.IRON_ORE, 25)),
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 100))
    ),
    COPPER_INGOT_ALT_PURE_COPPER_INGOT(
        TransformerBuilding.REFINERY,
        "[A] Pure Copper Ingot",
        true,
        arrayOf(Rate(RawItem.COPPER_ORE, 15), Rate(RawItem.WATER, 10)),
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 37.5))
    ),
    COPPER_POWDER(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Copper Powder ",
        false,
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 300)),
        arrayOf(Rate(CraftableItem.COPPER_POWDER, 50))
    ),
    COPPER_SHEET(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Copper Sheet ",
        false,
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 20)),
        arrayOf(Rate(CraftableItem.COPPER_SHEET, 10))
    ),
    COPPER_SHEET_ALT_STEAMED_COPPER_SHEET(
        TransformerBuilding.REFINERY,
        "[A] Steamed Copper Sheet",
        true,
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 22.5), Rate(RawItem.WATER, 22.5)),
        arrayOf(Rate(CraftableItem.COPPER_SHEET, 22.5))
    ),
    CRUDE_OIL_ALT_UNPACKAGE_OIL(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Oil",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_OIL, 60)),
        arrayOf(Rate(RawItem.CRUDE_OIL, 60), Rate(CraftableItem.EMPTY_CANISTER, 60))
    ),
    CRYSTAL_OSCILLATOR(
        TransformerBuilding.MANUFACTURER,
        "[S] Crystal Oscillator ",
        false,
        arrayOf(Rate(CraftableItem.QUARTZ_CRYSTAL, 18), Rate(CraftableItem.CABLE, 14), Rate(CraftableItem.REINFORCED_IRON_PLATE, 2.5)),
        arrayOf(Rate(CraftableItem.CRYSTAL_OSCILLATOR, 1))
    ),
    CRYSTAL_OSCILLATOR_ALT_INSULATED_CRYSTAL_OSCILLATOR(
        TransformerBuilding.MANUFACTURER,
        "[A] Insulated Crystal Oscillator",
        true,
        arrayOf(Rate(CraftableItem.QUARTZ_CRYSTAL, 18.75), Rate(CraftableItem.RUBBER, 13.125), Rate(CraftableItem.AI_LIMITER, 1.875)),
        arrayOf(Rate(CraftableItem.CRYSTAL_OSCILLATOR, 1.9))
    ),
    ELECTROMAGNETIC_CONTROL_ROD(
        TransformerBuilding.ASSEMBLER,
        "[S] Electromagnetic Control Rod ",
        false,
        arrayOf(Rate(CraftableItem.STATOR, 6), Rate(CraftableItem.AI_LIMITER, 4)),
        arrayOf(Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 4))
    ),
    ELECTROMAGNETIC_CONTROL_ROD_ALT_ELECTROMAGNETIC_CONNECTION_ROD(
        TransformerBuilding.ASSEMBLER,
        "[A] Electromagnetic Connection Rod",
        true,
        arrayOf(Rate(CraftableItem.STATOR, 8), Rate(CraftableItem.HIGH_SPEED_CONNECTOR, 4)),
        arrayOf(Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 8))
    ),
    EMPTY_CANISTER(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Empty Canister ",
        false,
        arrayOf(Rate(CraftableItem.PLASTIC, 30)),
        arrayOf(Rate(CraftableItem.EMPTY_CANISTER, 60))
    ),
    EMPTY_CANISTER_ALT_COATED_IRON_CANISTER(
        TransformerBuilding.ASSEMBLER,
        "[A] Coated Iron Canister",
        true,
        arrayOf(Rate(CraftableItem.IRON_PLATE, 30), Rate(CraftableItem.COPPER_SHEET, 15)),
        arrayOf(Rate(CraftableItem.EMPTY_CANISTER, 60))
    ),
    EMPTY_CANISTER_ALT_STEEL_CANISTER(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Steel Canister",
        true,
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 60)),
        arrayOf(Rate(CraftableItem.EMPTY_CANISTER, 40))
    ),
    EMPTY_FLUID_TANK(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Empty Fluid Tank ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINUM_INGOT, 60)),
        arrayOf(Rate(CraftableItem.EMPTY_FLUID_TANK, 60))
    ),
    ENCASED_INDUSTRIAL_BEAM(
        TransformerBuilding.ASSEMBLER,
        "[S] Encased Industrial Beam ",
        false,
        arrayOf(Rate(CraftableItem.STEEL_BEAM, 24), Rate(CraftableItem.CONCRETE, 30)),
        arrayOf(Rate(CraftableItem.ENCASED_INDUSTRIAL_BEAM, 6))
    ),
    ENCASED_INDUSTRIAL_BEAM_ALT_ENCASED_INDUSTRIAL_PIPE(
        TransformerBuilding.ASSEMBLER,
        "[A] Encased Industrial Pipe",
        true,
        arrayOf(Rate(CraftableItem.STEEL_PIPE, 28), Rate(CraftableItem.CONCRETE, 20)),
        arrayOf(Rate(CraftableItem.ENCASED_INDUSTRIAL_BEAM, 4))
    ),
    ENCASED_PLUTONIUM_CELL(
        TransformerBuilding.ASSEMBLER,
        "[S] Encased Plutonium Cell ",
        false,
        arrayOf(Rate(CraftableItem.PLUTONIUM_PELLET, 10), Rate(CraftableItem.CONCRETE, 20)),
        arrayOf(Rate(CraftableItem.ENCASED_PLUTONIUM_CELL, 5))
    ),
    ENCASED_PLUTONIUM_CELL_ALT_INSTANT_PLUTONIUM_CELL(
        TransformerBuilding.PARTICLE_ACCELERATOR,
        "[A] Instant Plutonium Cell",
        true,
        arrayOf(Rate(CraftableItem.NON_FISSILE_URANIUM, 75), Rate(CraftableItem.ALUMINUM_CASING, 10)),
        arrayOf(Rate(CraftableItem.ENCASED_PLUTONIUM_CELL, 10))
    ),
    ENCASED_URANIUM_CELL(
        TransformerBuilding.BLENDER,
        "[S] Encased Uranium Cell ",
        false,
        arrayOf(Rate(RawItem.URANIUM, 50), Rate(CraftableItem.CONCRETE, 15), Rate(CraftableItem.SULFURIC_ACID, 40)),
        arrayOf(Rate(CraftableItem.ENCASED_URANIUM_CELL, 25), Rate(CraftableItem.SULFURIC_ACID, 10))
    ),
    ENCASED_URANIUM_CELL_ALT_INFUSED_URANIUM_CELL(
        TransformerBuilding.MANUFACTURER,
        "[A] Infused Uranium Cell",
        true,
        arrayOf(Rate(RawItem.URANIUM, 25), Rate(CraftableItem.SILICA, 15), Rate(RawItem.SULFUR, 25), Rate(CraftableItem.QUICKWIRE, 75)),
        arrayOf(Rate(CraftableItem.ENCASED_URANIUM_CELL, 20))
    ),
    FABRIC(
        TransformerBuilding.ASSEMBLER,
        "[S] Fabric ",
        false,
        arrayOf(Rate(RawItem.MYCELIA, 15), Rate(CraftableItem.BIOMASS, 75)),
        arrayOf(Rate(CraftableItem.FABRIC, 15))
    ),
    FABRIC_ALT_POLYESTER_FABRIC(
        TransformerBuilding.REFINERY,
        "[A] Polyester Fabric",
        true,
        arrayOf(Rate(CraftableItem.POLYMER_RESIN, 80), Rate(RawItem.WATER, 50)),
        arrayOf(Rate(CraftableItem.FABRIC, 5))
    ),
    FUEL(
        TransformerBuilding.REFINERY,
        "[S] Fuel ",
        false,
        arrayOf(Rate(RawItem.CRUDE_OIL, 60)),
        arrayOf(Rate(CraftableItem.FUEL, 40), Rate(CraftableItem.POLYMER_RESIN, 30))
    ),
    FUEL_ALT_DILUTED_FUEL(
        TransformerBuilding.BLENDER,
        "[A] Diluted Fuel",
        true,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 50), Rate(RawItem.WATER, 100)),
        arrayOf(Rate(CraftableItem.FUEL, 100))
    ),
    FUEL_ALT_RESIDUAL_FUEL(
        TransformerBuilding.REFINERY,
        "[A] Residual Fuel",
        true,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 60)),
        arrayOf(Rate(CraftableItem.FUEL, 40))
    ),
    FUEL_ALT_UNPACKAGE_FUEL(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Fuel",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_FUEL, 60)),
        arrayOf(Rate(CraftableItem.FUEL, 60), Rate(CraftableItem.EMPTY_CANISTER, 60))
    ),
    FUSED_MODULAR_FRAME(
        TransformerBuilding.BLENDER,
        "[S] Fused Modular Frame ",
        false,
        arrayOf(Rate(CraftableItem.HEAVY_MODULAR_FRAME, 1.5), Rate(CraftableItem.ALUMINUM_CASING, 75), Rate(RawItem.NITROGEN_GAS, 37.5)),
        arrayOf(Rate(CraftableItem.FUSED_MODULAR_FRAME, 1.5))
    ),
    FUSED_MODULAR_FRAME_ALT_HEAT_FUSED_FRAME(
        TransformerBuilding.BLENDER,
        "[A] Heat-Fused Frame",
        true,
        arrayOf(Rate(CraftableItem.HEAVY_MODULAR_FRAME, 3), Rate(CraftableItem.ALUMINUM_INGOT, 150), Rate(CraftableItem.NITRIC_ACID, 24), Rate(CraftableItem.FUEL, 30)),
        arrayOf(Rate(CraftableItem.FUSED_MODULAR_FRAME, 3))
    ),
    GAS_FILTER(
        TransformerBuilding.MANUFACTURER,
        "[S] Gas Filter ",
        false,
        arrayOf(Rate(RawItem.COAL, 37.5), Rate(CraftableItem.RUBBER, 15), Rate(CraftableItem.FABRIC, 15)),
        arrayOf(Rate(CraftableItem.GAS_FILTER, 7.5))
    ),
    HEAT_SINK(
        TransformerBuilding.ASSEMBLER,
        "[S] Heat Sink ",
        false,
        arrayOf(Rate(CraftableItem.ALCLAD_ALUMINUM_SHEET, 37.5), Rate(CraftableItem.COPPER_SHEET, 22.5)),
        arrayOf(Rate(CraftableItem.HEAT_SINK, 7.5))
    ),
    HEAT_SINK_ALT_HEAT_EXCHANGER(
        TransformerBuilding.ASSEMBLER,
        "[A] Heat Exchanger",
        true,
        arrayOf(Rate(CraftableItem.ALUMINUM_CASING, 30), Rate(CraftableItem.RUBBER, 30)),
        arrayOf(Rate(CraftableItem.HEAT_SINK, 10))
    ),
    HEAVY_MODULAR_FRAME(
        TransformerBuilding.MANUFACTURER,
        "[S] Heavy Modular Frame ",
        false,
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 10), Rate(CraftableItem.STEEL_PIPE, 30), Rate(CraftableItem.ENCASED_INDUSTRIAL_BEAM, 10), Rate(CraftableItem.SCREW, 200)),
        arrayOf(Rate(CraftableItem.HEAVY_MODULAR_FRAME, 2))
    ),
    HEAVY_MODULAR_FRAME_ALT_HEAVY_ENCASED_FRAME(
        TransformerBuilding.MANUFACTURER,
        "[A] Heavy Encased Frame",
        true,
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 7.5), Rate(CraftableItem.ENCASED_INDUSTRIAL_BEAM, 9.375), Rate(CraftableItem.STEEL_PIPE, 33.75), Rate(CraftableItem.CONCRETE, 20.625)),
        arrayOf(Rate(CraftableItem.HEAVY_MODULAR_FRAME, 2.8))
    ),
    HEAVY_MODULAR_FRAME_ALT_HEAVY_FLEXIBLE_FRAME(
        TransformerBuilding.MANUFACTURER,
        "[A] Heavy Flexible Frame",
        true,
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 18.75), Rate(CraftableItem.ENCASED_INDUSTRIAL_BEAM, 11.25), Rate(CraftableItem.RUBBER, 75), Rate(CraftableItem.SCREW, 390)),
        arrayOf(Rate(CraftableItem.HEAVY_MODULAR_FRAME, 3.8))
    ),
    HEAVY_OIL_RESIDUE_ALT_HEAVY_OIL_RESIDUE_STANDARD(
        TransformerBuilding.REFINERY,
        "[A] Heavy Oil Residue ",
        true,
        arrayOf(Rate(RawItem.CRUDE_OIL, 30)),
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 40), Rate(CraftableItem.POLYMER_RESIN, 20))
    ),
    HEAVY_OIL_RESIDUE_ALT_UNPACKAGE_HEAVY_OIL_RESIDUE(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Heavy Oil Residue",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_HEAVY_OIL_RESIDUE, 20)),
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 20), Rate(CraftableItem.EMPTY_CANISTER, 20))
    ),
    HIGH_SPEED_CONNECTOR(
        TransformerBuilding.MANUFACTURER,
        "[S] High-Speed Connector ",
        false,
        arrayOf(Rate(CraftableItem.QUICKWIRE, 210), Rate(CraftableItem.CABLE, 37.5), Rate(CraftableItem.CIRCUIT_BOARD, 3.75)),
        arrayOf(Rate(CraftableItem.HIGH_SPEED_CONNECTOR, 3.8))
    ),
    HIGH_SPEED_CONNECTOR_ALT_SILICON_HIGH_SPEED_CONNECTOR(
        TransformerBuilding.MANUFACTURER,
        "[A] Silicon High-Speed Connector",
        true,
        arrayOf(Rate(CraftableItem.QUICKWIRE, 90), Rate(CraftableItem.SILICA, 37.5), Rate(CraftableItem.CIRCUIT_BOARD, 3)),
        arrayOf(Rate(CraftableItem.HIGH_SPEED_CONNECTOR, 3))
    ),
    IODINE_INFUSED_FILTER(
        TransformerBuilding.MANUFACTURER,
        "[S] Iodine Infused Filter ",
        false,
        arrayOf(Rate(CraftableItem.GAS_FILTER, 3.75), Rate(CraftableItem.QUICKWIRE, 30), Rate(CraftableItem.ALUMINUM_CASING, 3.75)),
        arrayOf(Rate(CraftableItem.IODINE_INFUSED_FILTER, 3.8))
    ),
    IRON_INGOT(
        TransformerBuilding.SMELTER,
        "[S] Iron Ingot ",
        false,
        arrayOf(Rate(RawItem.IRON_ORE, 30)),
        arrayOf(Rate(CraftableItem.IRON_INGOT, 30))
    ),
    IRON_INGOT_ALT_IRON_ALLOY_INGOT(
        TransformerBuilding.FOUNDRY,
        "[A] Iron Alloy Ingot",
        true,
        arrayOf(Rate(RawItem.COPPER_ORE, 20), Rate(RawItem.IRON_ORE, 20)),
        arrayOf(Rate(CraftableItem.IRON_INGOT, 50))
    ),
    IRON_INGOT_ALT_PURE_IRON_INGOT(
        TransformerBuilding.REFINERY,
        "[A] Pure Iron Ingot",
        true,
        arrayOf(Rate(RawItem.IRON_ORE, 35), Rate(RawItem.WATER, 20)),
        arrayOf(Rate(CraftableItem.IRON_INGOT, 65))
    ),
    IRON_PLATE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Iron Plate ",
        false,
        arrayOf(Rate(CraftableItem.IRON_INGOT, 30)),
        arrayOf(Rate(CraftableItem.IRON_PLATE, 20))
    ),
    IRON_PLATE_ALT_COATED_IRON_PLATE(
        TransformerBuilding.ASSEMBLER,
        "[A] Coated Iron Plate",
        true,
        arrayOf(Rate(CraftableItem.IRON_INGOT, 50), Rate(CraftableItem.PLASTIC, 10)),
        arrayOf(Rate(CraftableItem.IRON_PLATE, 75))
    ),
    IRON_PLATE_ALT_STEEL_COATED_PLATE(
        TransformerBuilding.ASSEMBLER,
        "[A] Steel Coated Plate",
        true,
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 7.5), Rate(CraftableItem.PLASTIC, 5)),
        arrayOf(Rate(CraftableItem.IRON_PLATE, 45))
    ),
    IRON_ROD(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Iron Rod ",
        false,
        arrayOf(Rate(CraftableItem.IRON_INGOT, 15)),
        arrayOf(Rate(CraftableItem.IRON_ROD, 15))
    ),
    IRON_ROD_ALT_STEEL_ROD(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Steel Rod",
        true,
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 12)),
        arrayOf(Rate(CraftableItem.IRON_ROD, 48))
    ),
    LIQUID_BIOFUEL(
        TransformerBuilding.REFINERY,
        "[S] Liquid Biofuel ",
        false,
        arrayOf(Rate(CraftableItem.SOLID_BIOFUEL, 90), Rate(RawItem.WATER, 45)),
        arrayOf(Rate(CraftableItem.LIQUID_BIOFUEL, 60))
    ),
    LIQUID_BIOFUEL_ALT_UNPACKAGE_LIQUID_BIOFUEL(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Liquid Biofuel",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_LIQUID_BIOFUEL, 60)),
        arrayOf(Rate(CraftableItem.LIQUID_BIOFUEL, 60), Rate(CraftableItem.EMPTY_CANISTER, 60))
    ),
    MAGNETIC_FIELD_GENERATOR(
        TransformerBuilding.MANUFACTURER,
        "[S] Magnetic Field Generator ",
        false,
        arrayOf(Rate(CraftableItem.VERSATILE_FRAMEWORK, 2.5), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 1), Rate(CraftableItem.BATTERY, 5)),
        arrayOf(Rate(CraftableItem.MAGNETIC_FIELD_GENERATOR, 1))
    ),
    MODULAR_ENGINE(
        TransformerBuilding.MANUFACTURER,
        "[S] Modular Engine ",
        false,
        arrayOf(Rate(CraftableItem.MOTOR, 2), Rate(CraftableItem.RUBBER, 15), Rate(CraftableItem.SMART_PLATING, 2)),
        arrayOf(Rate(CraftableItem.MODULAR_ENGINE, 1))
    ),
    MODULAR_FRAME(
        TransformerBuilding.ASSEMBLER,
        "[S] Modular Frame ",
        false,
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 3), Rate(CraftableItem.IRON_ROD, 12)),
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 2))
    ),
    MODULAR_FRAME_ALT_BOLTED_FRAME(
        TransformerBuilding.ASSEMBLER,
        "[A] Bolted Frame",
        true,
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 7.5), Rate(CraftableItem.SCREW, 140)),
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 5))
    ),
    MODULAR_FRAME_ALT_STEELED_FRAME(
        TransformerBuilding.ASSEMBLER,
        "[A] Steeled Frame",
        true,
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 2), Rate(CraftableItem.STEEL_PIPE, 10)),
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 3))
    ),
    MOTOR(
        TransformerBuilding.ASSEMBLER,
        "[S] Motor ",
        false,
        arrayOf(Rate(CraftableItem.ROTOR, 10), Rate(CraftableItem.STATOR, 10)),
        arrayOf(Rate(CraftableItem.MOTOR, 5))
    ),
    MOTOR_ALT_ELECTRIC_MOTOR(
        TransformerBuilding.ASSEMBLER,
        "[A] Electric Motor",
        true,
        arrayOf(Rate(CraftableItem.ROTOR, 7.5), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 3.75)),
        arrayOf(Rate(CraftableItem.MOTOR, 7.5))
    ),
    MOTOR_ALT_RIGOUR_MOTOR(
        TransformerBuilding.MANUFACTURER,
        "[A] Rigour Motor",
        true,
        arrayOf(Rate(CraftableItem.ROTOR, 3.75), Rate(CraftableItem.STATOR, 3.75), Rate(CraftableItem.CRYSTAL_OSCILLATOR, 1.25)),
        arrayOf(Rate(CraftableItem.MOTOR, 7.5))
    ),
    NITRIC_ACID(
        TransformerBuilding.BLENDER,
        "[S] Nitric Acid ",
        false,
        arrayOf(Rate(RawItem.NITROGEN_GAS, 120), Rate(RawItem.WATER, 30), Rate(CraftableItem.IRON_PLATE, 10)),
        arrayOf(Rate(CraftableItem.NITRIC_ACID, 30))
    ),
    NITRIC_ACID_ALT_UNPACKAGE_NITRIC_ACID(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Nitric Acid",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_NITRIC_ACID, 20)),
        arrayOf(Rate(CraftableItem.NITRIC_ACID, 20), Rate(CraftableItem.EMPTY_CANISTER, 20))
    ),
    NITROGEN_GAS_ALT_UNPACKAGE_NITROGEN_GAS(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Nitrogen Gas",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_NITROGEN_GAS, 60)),
        arrayOf(Rate(RawItem.NITROGEN_GAS, 240), Rate(CraftableItem.EMPTY_FLUID_TANK, 60))
    ),
    NOBELISK(
        TransformerBuilding.ASSEMBLER,
        "[S] Nobelisk ",
        false,
        arrayOf(Rate(CraftableItem.BLACK_POWDER, 15), Rate(CraftableItem.STEEL_PIPE, 30)),
        arrayOf(Rate(CraftableItem.NOBELISK, 3))
    ),
    NOBELISK_ALT_SEISMIC_NOBELISK(
        TransformerBuilding.MANUFACTURER,
        "[A] Seismic Nobelisk",
        true,
        arrayOf(Rate(CraftableItem.BLACK_POWDER, 12), Rate(CraftableItem.STEEL_PIPE, 12), Rate(CraftableItem.CRYSTAL_OSCILLATOR, 1.5)),
        arrayOf(Rate(CraftableItem.NOBELISK, 6))
    ),
    NON_FISSILE_URANIUM(
        TransformerBuilding.BLENDER,
        "[S] Non-fissile Uranium ",
        false,
        arrayOf(Rate(CraftableItem.URANIUM_WASTE, 37.5), Rate(CraftableItem.SILICA, 25), Rate(CraftableItem.NITRIC_ACID, 15), Rate(CraftableItem.SULFURIC_ACID, 15)),
        arrayOf(Rate(CraftableItem.NON_FISSILE_URANIUM, 50), Rate(RawItem.WATER, 15))
    ),
    NON_FISSILE_URANIUM_ALT_FERTILE_URANIUM(
        TransformerBuilding.BLENDER,
        "[A] Fertile Uranium",
        true,
        arrayOf(Rate(RawItem.URANIUM, 25), Rate(CraftableItem.URANIUM_WASTE, 25), Rate(CraftableItem.NITRIC_ACID, 15), Rate(CraftableItem.SULFURIC_ACID, 25)),
        arrayOf(Rate(CraftableItem.NON_FISSILE_URANIUM, 100), Rate(RawItem.WATER, 40))
    ),
    NUCLEAR_PASTA(
        TransformerBuilding.PARTICLE_ACCELERATOR,
        "[S] Nuclear Pasta ",
        false,
        arrayOf(Rate(CraftableItem.COPPER_POWDER, 100), Rate(CraftableItem.PRESSURE_CONVERSION_CUBE, 0.5)),
        arrayOf(Rate(CraftableItem.NUCLEAR_PASTA, 0.5))
    ),
    PACKAGED_ALUMINA_SOLUTION(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Alumina Solution ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINA_SOLUTION, 120), Rate(CraftableItem.EMPTY_CANISTER, 120)),
        arrayOf(Rate(CraftableItem.PACKAGED_ALUMINA_SOLUTION, 120))
    ),
    PACKAGED_FUEL(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Fuel ",
        false,
        arrayOf(Rate(CraftableItem.FUEL, 40), Rate(CraftableItem.EMPTY_CANISTER, 40)),
        arrayOf(Rate(CraftableItem.PACKAGED_FUEL, 40))
    ),
    PACKAGED_FUEL_ALT_DILUTED_PACKAGED_FUEL(
        TransformerBuilding.REFINERY,
        "[A] Diluted Packaged Fuel",
        true,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 30), Rate(CraftableItem.PACKAGED_WATER, 60)),
        arrayOf(Rate(CraftableItem.PACKAGED_FUEL, 60))
    ),
    PACKAGED_HEAVY_OIL_RESIDUE(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Heavy Oil Residue ",
        false,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 30), Rate(CraftableItem.EMPTY_CANISTER, 30)),
        arrayOf(Rate(CraftableItem.PACKAGED_HEAVY_OIL_RESIDUE, 30))
    ),
    PACKAGED_LIQUID_BIOFUEL(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Liquid Biofuel ",
        false,
        arrayOf(Rate(CraftableItem.LIQUID_BIOFUEL, 40), Rate(CraftableItem.EMPTY_CANISTER, 40)),
        arrayOf(Rate(CraftableItem.PACKAGED_LIQUID_BIOFUEL, 40))
    ),
    PACKAGED_NITRIC_ACID(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Nitric Acid ",
        false,
        arrayOf(Rate(CraftableItem.NITRIC_ACID, 30), Rate(CraftableItem.EMPTY_CANISTER, 30)),
        arrayOf(Rate(CraftableItem.PACKAGED_NITRIC_ACID, 30))
    ),
    PACKAGED_NITROGEN_GAS(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Nitrogen Gas ",
        false,
        arrayOf(Rate(RawItem.NITROGEN_GAS, 240), Rate(CraftableItem.EMPTY_FLUID_TANK, 60)),
        arrayOf(Rate(CraftableItem.PACKAGED_NITROGEN_GAS, 60))
    ),
    PACKAGED_OIL(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Oil ",
        false,
        arrayOf(Rate(RawItem.CRUDE_OIL, 30), Rate(CraftableItem.EMPTY_CANISTER, 30)),
        arrayOf(Rate(CraftableItem.PACKAGED_OIL, 30))
    ),
    PACKAGED_SULFURIC_ACID(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Sulfuric Acid ",
        false,
        arrayOf(Rate(CraftableItem.SULFURIC_ACID, 40), Rate(CraftableItem.EMPTY_CANISTER, 40)),
        arrayOf(Rate(CraftableItem.PACKAGED_SULFURIC_ACID, 40))
    ),
    PACKAGED_TURBOFUEL(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Turbofuel ",
        false,
        arrayOf(Rate(CraftableItem.TURBOFUEL, 20), Rate(CraftableItem.EMPTY_CANISTER, 20)),
        arrayOf(Rate(CraftableItem.PACKAGED_TURBOFUEL, 20))
    ),
    PACKAGED_WATER(
        TransformerBuilding.PACKAGER,
        "[S] Packaged Water ",
        false,
        arrayOf(Rate(RawItem.WATER, 60), Rate(CraftableItem.EMPTY_CANISTER, 60)),
        arrayOf(Rate(CraftableItem.PACKAGED_WATER, 60))
    ),
    PETROLEUM_COKE(
        TransformerBuilding.REFINERY,
        "[S] Petroleum Coke ",
        false,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 40)),
        arrayOf(Rate(CraftableItem.PETROLEUM_COKE, 120))
    ),
    PLASTIC(
        TransformerBuilding.REFINERY,
        "[S] Plastic ",
        false,
        arrayOf(Rate(RawItem.CRUDE_OIL, 30)),
        arrayOf(Rate(CraftableItem.PLASTIC, 20), Rate(CraftableItem.HEAVY_OIL_RESIDUE, 10))
    ),
    PLASTIC_ALT_RECYCLED_PLASTIC(
        TransformerBuilding.REFINERY,
        "[A] Recycled Plastic",
        true,
        arrayOf(Rate(CraftableItem.RUBBER, 30), Rate(CraftableItem.FUEL, 30)),
        arrayOf(Rate(CraftableItem.PLASTIC, 60))
    ),
    PLASTIC_ALT_RESIDUAL_PLASTIC(
        TransformerBuilding.REFINERY,
        "[A] Residual Plastic",
        true,
        arrayOf(Rate(CraftableItem.POLYMER_RESIN, 60), Rate(RawItem.WATER, 20)),
        arrayOf(Rate(CraftableItem.PLASTIC, 20))
    ),
    PLUTONIUM_FUEL_ROD(
        TransformerBuilding.MANUFACTURER,
        "[S] Plutonium Fuel Rod ",
        false,
        arrayOf(Rate(CraftableItem.ENCASED_PLUTONIUM_CELL, 7.5), Rate(CraftableItem.STEEL_BEAM, 4.5), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 1.5), Rate(CraftableItem.HEAT_SINK, 2.5)),
        arrayOf(Rate(CraftableItem.PLUTONIUM_FUEL_ROD, 0.3))
    ),
    PLUTONIUM_FUEL_ROD_ALT_PLUTONIUM_FUEL_UNIT(
        TransformerBuilding.ASSEMBLER,
        "[A] Plutonium Fuel Unit",
        true,
        arrayOf(Rate(CraftableItem.ENCASED_PLUTONIUM_CELL, 10), Rate(CraftableItem.PRESSURE_CONVERSION_CUBE, 0.5)),
        arrayOf(Rate(CraftableItem.PLUTONIUM_FUEL_ROD, 0.5))
    ),
    PLUTONIUM_PELLET(
        TransformerBuilding.PARTICLE_ACCELERATOR,
        "[S] Plutonium Pellet ",
        false,
        arrayOf(Rate(CraftableItem.NON_FISSILE_URANIUM, 100), Rate(CraftableItem.URANIUM_WASTE, 25)),
        arrayOf(Rate(CraftableItem.PLUTONIUM_PELLET, 30))
    ),
    PLUTONIUM_WASTE(
        TransformerBuilding.NUCLEAR_POWER_PLANT,
        "[S] Plutonium Waste ",
        false,
        arrayOf(Rate(CraftableItem.PLUTONIUM_FUEL_ROD, 0.1), Rate(RawItem.WATER, 300)),
        arrayOf(Rate(CraftableItem.PLUTONIUM_WASTE, 1))
    ),
    POLYMER_RESIN_ALT_POLYMER_RESIN_STANDARD(
        TransformerBuilding.REFINERY,
        "[A] Polymer Resin ",
        true,
        arrayOf(Rate(RawItem.CRUDE_OIL, 60)),
        arrayOf(Rate(CraftableItem.POLYMER_RESIN, 130), Rate(CraftableItem.HEAVY_OIL_RESIDUE, 20))
    ),
    PORTABLE_MINER(
        TransformerBuilding.MANUFACTURER,
        "[S] Automated Miner",
        false,
        arrayOf(Rate(CraftableItem.MOTOR, 1), Rate(CraftableItem.STEEL_PIPE, 4), Rate(CraftableItem.IRON_ROD, 4), Rate(CraftableItem.IRON_PLATE, 2)),
        arrayOf(Rate(CraftableItem.PORTABLE_MINER, 1))
    ),
    POWER_SHARD(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Power Shard ",
        false,
        arrayOf(Rate(RawItem.GREEN_POWER_SLUG, 7.5)),
        arrayOf(Rate(CraftableItem.POWER_SHARD, 7.5))
    ),
    PRESSURE_CONVERSION_CUBE(
        TransformerBuilding.ASSEMBLER,
        "[S] Pressure Conversion Cube ",
        false,
        arrayOf(Rate(CraftableItem.FUSED_MODULAR_FRAME, 1), Rate(CraftableItem.RADIO_CONTROL_UNIT, 2)),
        arrayOf(Rate(CraftableItem.PRESSURE_CONVERSION_CUBE, 1))
    ),
    QUARTZ_CRYSTAL(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Quartz Crystal ",
        false,
        arrayOf(Rate(RawItem.RAW_QUARTZ, 37.5)),
        arrayOf(Rate(CraftableItem.QUARTZ_CRYSTAL, 22.5))
    ),
    QUARTZ_CRYSTAL_ALT_PURE_QUARTZ_CRYSTAL(
        TransformerBuilding.REFINERY,
        "[A] Pure Quartz Crystal",
        true,
        arrayOf(Rate(RawItem.RAW_QUARTZ, 67.5), Rate(RawItem.WATER, 37.5)),
        arrayOf(Rate(CraftableItem.QUARTZ_CRYSTAL, 52.5))
    ),
    QUICKWIRE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Quickwire ",
        false,
        arrayOf(Rate(CraftableItem.CATERIUM_INGOT, 12)),
        arrayOf(Rate(CraftableItem.QUICKWIRE, 60))
    ),
    QUICKWIRE_ALT_FUSED_QUICKWIRE(
        TransformerBuilding.ASSEMBLER,
        "[A] Fused Quickwire",
        true,
        arrayOf(Rate(CraftableItem.CATERIUM_INGOT, 7.5), Rate(CraftableItem.COPPER_INGOT, 37.5)),
        arrayOf(Rate(CraftableItem.QUICKWIRE, 90))
    ),
    RADIO_CONTROL_UNIT(
        TransformerBuilding.MANUFACTURER,
        "[S] Radio Control Unit ",
        false,
        arrayOf(Rate(CraftableItem.ALUMINUM_CASING, 40), Rate(CraftableItem.CRYSTAL_OSCILLATOR, 1.25), Rate(CraftableItem.COMPUTER, 1.25)),
        arrayOf(Rate(CraftableItem.RADIO_CONTROL_UNIT, 2.5))
    ),
    RADIO_CONTROL_UNIT_ALT_RADIO_CONNECTION_UNIT(
        TransformerBuilding.MANUFACTURER,
        "[A] Radio Connection Unit",
        true,
        arrayOf(Rate(CraftableItem.HEAT_SINK, 15), Rate(CraftableItem.HIGH_SPEED_CONNECTOR, 7.5), Rate(CraftableItem.QUARTZ_CRYSTAL, 45)),
        arrayOf(Rate(CraftableItem.RADIO_CONTROL_UNIT, 3.8))
    ),
    RADIO_CONTROL_UNIT_ALT_RADIO_CONTROL_SYSTEM(
        TransformerBuilding.MANUFACTURER,
        "[A] Radio Control System",
        true,
        arrayOf(Rate(CraftableItem.CRYSTAL_OSCILLATOR, 1.5), Rate(CraftableItem.CIRCUIT_BOARD, 15), Rate(CraftableItem.ALUMINUM_CASING, 90), Rate(CraftableItem.RUBBER, 45)),
        arrayOf(Rate(CraftableItem.RADIO_CONTROL_UNIT, 4.5))
    ),
    REINFORCED_IRON_PLATE(
        TransformerBuilding.ASSEMBLER,
        "[S] Reinforced Iron Plate ",
        false,
        arrayOf(Rate(CraftableItem.IRON_PLATE, 30), Rate(CraftableItem.SCREW, 60)),
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 5))
    ),
    REINFORCED_IRON_PLATE_ALT_ADHERED_IRON_PLATE(
        TransformerBuilding.ASSEMBLER,
        "[A] Adhered Iron Plate",
        true,
        arrayOf(Rate(CraftableItem.IRON_PLATE, 11.25), Rate(CraftableItem.RUBBER, 3.75)),
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 3.8))
    ),
    REINFORCED_IRON_PLATE_ALT_BOLTED_IRON_PLATE(
        TransformerBuilding.ASSEMBLER,
        "[A] Bolted Iron Plate",
        true,
        arrayOf(Rate(CraftableItem.IRON_PLATE, 90), Rate(CraftableItem.SCREW, 250)),
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 15))
    ),
    REINFORCED_IRON_PLATE_ALT_STITCHED_IRON_PLATE(
        TransformerBuilding.ASSEMBLER,
        "[A] Stitched Iron Plate",
        true,
        arrayOf(Rate(CraftableItem.IRON_PLATE, 18.75), Rate(CraftableItem.WIRE, 37.5)),
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 5.6))
    ),
    RIFLE_CARTRIDGE(
        TransformerBuilding.MANUFACTURER,
        "[S] Rifle Cartridge ",
        false,
        arrayOf(Rate(CraftableItem.BEACON, 3), Rate(CraftableItem.STEEL_PIPE, 30), Rate(CraftableItem.BLACK_POWDER, 30), Rate(CraftableItem.RUBBER, 30)),
        arrayOf(Rate(CraftableItem.RIFLE_CARTRIDGE, 15))
    ),
    ROTOR(
        TransformerBuilding.ASSEMBLER,
        "[S] Rotor ",
        false,
        arrayOf(Rate(CraftableItem.IRON_ROD, 20), Rate(CraftableItem.SCREW, 100)),
        arrayOf(Rate(CraftableItem.ROTOR, 4))
    ),
    ROTOR_ALT_COPPER_ROTOR(
        TransformerBuilding.ASSEMBLER,
        "[A] Copper Rotor",
        true,
        arrayOf(Rate(CraftableItem.COPPER_SHEET, 22.5), Rate(CraftableItem.SCREW, 195)),
        arrayOf(Rate(CraftableItem.ROTOR, 11.3))
    ),
    ROTOR_ALT_STEEL_ROTOR(
        TransformerBuilding.ASSEMBLER,
        "[A] Steel Rotor",
        true,
        arrayOf(Rate(CraftableItem.STEEL_PIPE, 10), Rate(CraftableItem.WIRE, 30)),
        arrayOf(Rate(CraftableItem.ROTOR, 5))
    ),
    RUBBER(
        TransformerBuilding.REFINERY,
        "[S] Rubber ",
        false,
        arrayOf(Rate(RawItem.CRUDE_OIL, 30)),
        arrayOf(Rate(CraftableItem.RUBBER, 20), Rate(CraftableItem.HEAVY_OIL_RESIDUE, 20))
    ),
    RUBBER_ALT_RECYCLED_RUBBER(
        TransformerBuilding.REFINERY,
        "[A] Recycled Rubber",
        true,
        arrayOf(Rate(CraftableItem.PLASTIC, 30), Rate(CraftableItem.FUEL, 30)),
        arrayOf(Rate(CraftableItem.RUBBER, 60))
    ),
    RUBBER_ALT_RESIDUAL_RUBBER(
        TransformerBuilding.REFINERY,
        "[A] Residual Rubber",
        true,
        arrayOf(Rate(CraftableItem.POLYMER_RESIN, 40), Rate(RawItem.WATER, 20)),
        arrayOf(Rate(CraftableItem.RUBBER, 20))
    ),
    SCREW(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Screw ",
        false,
        arrayOf(Rate(CraftableItem.IRON_ROD, 10)),
        arrayOf(Rate(CraftableItem.SCREW, 40))
    ),
    SCREW_ALT_CAST_SCREW(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Cast Screw",
        true,
        arrayOf(Rate(CraftableItem.IRON_INGOT, 12.5)),
        arrayOf(Rate(CraftableItem.SCREW, 50))
    ),
    SCREW_ALT_STEEL_SCREW(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Steel Screw",
        true,
        arrayOf(Rate(CraftableItem.STEEL_BEAM, 5)),
        arrayOf(Rate(CraftableItem.SCREW, 260))
    ),
    SILICA(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Silica ",
        false,
        arrayOf(Rate(RawItem.RAW_QUARTZ, 22.5)),
        arrayOf(Rate(CraftableItem.SILICA, 37.5))
    ),
    SILICA_ALT_CHEAP_SILICA(
        TransformerBuilding.ASSEMBLER,
        "[A] Cheap Silica",
        true,
        arrayOf(Rate(RawItem.RAW_QUARTZ, 11.25), Rate(RawItem.LIMESTONE, 18.75)),
        arrayOf(Rate(CraftableItem.SILICA, 26.3))
    ),
    SMART_PLATING(
        TransformerBuilding.ASSEMBLER,
        "[S] Smart Plating ",
        false,
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 2), Rate(CraftableItem.ROTOR, 2)),
        arrayOf(Rate(CraftableItem.SMART_PLATING, 2))
    ),
    SMART_PLATING_ALT_PLASTIC_SMART_PLATING(
        TransformerBuilding.MANUFACTURER,
        "[A] Plastic Smart Plating",
        true,
        arrayOf(Rate(CraftableItem.REINFORCED_IRON_PLATE, 2.5), Rate(CraftableItem.ROTOR, 2.5), Rate(CraftableItem.PLASTIC, 7.5)),
        arrayOf(Rate(CraftableItem.SMART_PLATING, 5))
    ),
    SOLID_BIOFUEL(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Solid Biofuel ",
        false,
        arrayOf(Rate(CraftableItem.BIOMASS, 120)),
        arrayOf(Rate(CraftableItem.SOLID_BIOFUEL, 60))
    ),
    SPIKED_REBAR(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Spiked Rebar ",
        false,
        arrayOf(Rate(CraftableItem.IRON_ROD, 15)),
        arrayOf(Rate(CraftableItem.SPIKED_REBAR, 15))
    ),
    STATOR(
        TransformerBuilding.ASSEMBLER,
        "[S] Stator ",
        false,
        arrayOf(Rate(CraftableItem.STEEL_PIPE, 15), Rate(CraftableItem.WIRE, 40)),
        arrayOf(Rate(CraftableItem.STATOR, 5))
    ),
    STATOR_ALT_QUICKWIRE_STATOR(
        TransformerBuilding.ASSEMBLER,
        "[A] Quickwire Stator",
        true,
        arrayOf(Rate(CraftableItem.STEEL_PIPE, 16), Rate(CraftableItem.QUICKWIRE, 60)),
        arrayOf(Rate(CraftableItem.STATOR, 8))
    ),
    STEEL_BEAM(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Steel Beam ",
        false,
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 60)),
        arrayOf(Rate(CraftableItem.STEEL_BEAM, 15))
    ),
    STEEL_INGOT(
        TransformerBuilding.FOUNDRY,
        "[S] Steel Ingot ",
        false,
        arrayOf(Rate(RawItem.IRON_ORE, 45), Rate(RawItem.COAL, 45)),
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 45))
    ),
    STEEL_INGOT_ALT_COKE_STEEL_INGOT(
        TransformerBuilding.FOUNDRY,
        "[A] Coke Steel Ingot",
        true,
        arrayOf(Rate(RawItem.IRON_ORE, 75), Rate(CraftableItem.PETROLEUM_COKE, 75)),
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 100))
    ),
    STEEL_INGOT_ALT_COMPACTED_STEEL_INGOT(
        TransformerBuilding.FOUNDRY,
        "[A] Compacted Steel Ingot",
        true,
        arrayOf(Rate(RawItem.IRON_ORE, 22.5), Rate(CraftableItem.COMPACTED_COAL, 11.25)),
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 37.5))
    ),
    STEEL_INGOT_ALT_SOLID_STEEL_INGOT(
        TransformerBuilding.FOUNDRY,
        "[A] Solid Steel Ingot",
        true,
        arrayOf(Rate(CraftableItem.IRON_INGOT, 40), Rate(RawItem.COAL, 40)),
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 60))
    ),
    STEEL_PIPE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Steel Pipe ",
        false,
        arrayOf(Rate(CraftableItem.STEEL_INGOT, 30)),
        arrayOf(Rate(CraftableItem.STEEL_PIPE, 20))
    ),
    SULFURIC_ACID(
        TransformerBuilding.REFINERY,
        "[S] Sulfuric Acid ",
        false,
        arrayOf(Rate(RawItem.SULFUR, 50), Rate(RawItem.WATER, 50)),
        arrayOf(Rate(CraftableItem.SULFURIC_ACID, 50))
    ),
    SULFURIC_ACID_ALT_UNPACKAGE_SULFURIC_ACID(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Sulfuric Acid",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_SULFURIC_ACID, 60)),
        arrayOf(Rate(CraftableItem.SULFURIC_ACID, 60), Rate(CraftableItem.EMPTY_CANISTER, 60))
    ),
    SUPERCOMPUTER(
        TransformerBuilding.MANUFACTURER,
        "[S] Supercomputer ",
        false,
        arrayOf(Rate(CraftableItem.COMPUTER, 3.75), Rate(CraftableItem.AI_LIMITER, 3.75), Rate(CraftableItem.HIGH_SPEED_CONNECTOR, 5.625), Rate(CraftableItem.PLASTIC, 52.5)),
        arrayOf(Rate(CraftableItem.SUPERCOMPUTER, 1.9))
    ),
    SUPERCOMPUTER_ALT_OC_SUPERCOMPUTER(
        TransformerBuilding.ASSEMBLER,
        "[A] OC Supercomputer",
        true,
        arrayOf(Rate(CraftableItem.RADIO_CONTROL_UNIT, 9), Rate(CraftableItem.COOLING_SYSTEM, 9)),
        arrayOf(Rate(CraftableItem.SUPERCOMPUTER, 3))
    ),
    SUPERCOMPUTER_ALT_SUPER_STATE_COMPUTER(
        TransformerBuilding.MANUFACTURER,
        "[A] Super-State Computer",
        true,
        arrayOf(Rate(CraftableItem.COMPUTER, 3.6), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 2.4), Rate(CraftableItem.BATTERY, 24), Rate(CraftableItem.WIRE, 54)),
        arrayOf(Rate(CraftableItem.SUPERCOMPUTER, 2.4))
    ),
    THERMAL_PROPULSION_ROCKET(
        TransformerBuilding.MANUFACTURER,
        "[S] Thermal Propulsion Rocket ",
        false,
        arrayOf(Rate(CraftableItem.MODULAR_ENGINE, 2.5), Rate(CraftableItem.TURBO_MOTOR, 1), Rate(CraftableItem.COOLING_SYSTEM, 3), Rate(CraftableItem.FUSED_MODULAR_FRAME, 1)),
        arrayOf(Rate(CraftableItem.THERMAL_PROPULSION_ROCKET, 1))
    ),
    TURBOFUEL_ALT_TURBOFUEL_STANDARD(
        TransformerBuilding.REFINERY,
        "[A] Turbofuel ",
        true,
        arrayOf(Rate(CraftableItem.FUEL, 22.5), Rate(CraftableItem.COMPACTED_COAL, 15)),
        arrayOf(Rate(CraftableItem.TURBOFUEL, 18.8))
    ),
    TURBOFUEL_ALT_TURBO_BLEND_FUEL(
        TransformerBuilding.BLENDER,
        "[A] Turbo Blend Fuel",
        true,
        arrayOf(Rate(CraftableItem.FUEL, 15), Rate(CraftableItem.HEAVY_OIL_RESIDUE, 30), Rate(RawItem.SULFUR, 22.5), Rate(CraftableItem.PETROLEUM_COKE, 22.5)),
        arrayOf(Rate(CraftableItem.TURBOFUEL, 45))
    ),
    TURBOFUEL_ALT_TURBO_HEAVY_FUEL(
        TransformerBuilding.REFINERY,
        "[A] Turbo Heavy Fuel",
        true,
        arrayOf(Rate(CraftableItem.HEAVY_OIL_RESIDUE, 37.5), Rate(CraftableItem.COMPACTED_COAL, 30)),
        arrayOf(Rate(CraftableItem.TURBOFUEL, 30))
    ),
    TURBOFUEL_ALT_UNPACKAGE_TURBOFUEL(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Turbofuel",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_TURBOFUEL, 20)),
        arrayOf(Rate(CraftableItem.TURBOFUEL, 20), Rate(CraftableItem.EMPTY_CANISTER, 20))
    ),
    TURBO_MOTOR(
        TransformerBuilding.MANUFACTURER,
        "[S] Turbo Motor ",
        false,
        arrayOf(Rate(CraftableItem.COOLING_SYSTEM, 7.5), Rate(CraftableItem.RADIO_CONTROL_UNIT, 3.75), Rate(CraftableItem.MOTOR, 7.5), Rate(CraftableItem.RUBBER, 45)),
        arrayOf(Rate(CraftableItem.TURBO_MOTOR, 1.9))
    ),
    TURBO_MOTOR_ALT_TURBO_ELECTRIC_MOTOR(
        TransformerBuilding.MANUFACTURER,
        "[A] Turbo Electric Motor",
        true,
        arrayOf(Rate(CraftableItem.MOTOR, 6.5625), Rate(CraftableItem.RADIO_CONTROL_UNIT, 8.4375), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 4.6875), Rate(CraftableItem.ROTOR, 6.5625)),
        arrayOf(Rate(CraftableItem.TURBO_MOTOR, 2.8))
    ),
    TURBO_MOTOR_ALT_TURBO_PRESSURE_MOTOR(
        TransformerBuilding.MANUFACTURER,
        "[A] Turbo Pressure Motor",
        true,
        arrayOf(Rate(CraftableItem.MOTOR, 7.5), Rate(CraftableItem.PRESSURE_CONVERSION_CUBE, 1.875), Rate(CraftableItem.PACKAGED_NITROGEN_GAS, 45), Rate(CraftableItem.STATOR, 15)),
        arrayOf(Rate(CraftableItem.TURBO_MOTOR, 3.8))
    ),
    URANIUM_FUEL_ROD(
        TransformerBuilding.MANUFACTURER,
        "[S] Uranium Fuel Rod ",
        false,
        arrayOf(Rate(CraftableItem.ENCASED_URANIUM_CELL, 20), Rate(CraftableItem.ENCASED_INDUSTRIAL_BEAM, 1.2), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 2)),
        arrayOf(Rate(CraftableItem.URANIUM_FUEL_ROD, 0.4))
    ),
    URANIUM_FUEL_ROD_ALT_URANIUM_FUEL_UNIT(
        TransformerBuilding.MANUFACTURER,
        "[A] Uranium Fuel Unit",
        true,
        arrayOf(Rate(CraftableItem.ENCASED_URANIUM_CELL, 20), Rate(CraftableItem.ELECTROMAGNETIC_CONTROL_ROD, 2), Rate(CraftableItem.CRYSTAL_OSCILLATOR, 0.6), Rate(CraftableItem.BEACON, 1.2)),
        arrayOf(Rate(CraftableItem.URANIUM_FUEL_ROD, 0.6))
    ),
    URANIUM_WASTE(
        TransformerBuilding.NUCLEAR_POWER_PLANT,
        "[S] Uranium Waste ",
        false,
        arrayOf(Rate(CraftableItem.URANIUM_FUEL_ROD, 0.2), Rate(RawItem.WATER, 300)),
        arrayOf(Rate(CraftableItem.URANIUM_WASTE, 10))
    ),
    VERSATILE_FRAMEWORK(
        TransformerBuilding.ASSEMBLER,
        "[S] Versatile Framework ",
        false,
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 2.5), Rate(CraftableItem.STEEL_BEAM, 30)),
        arrayOf(Rate(CraftableItem.VERSATILE_FRAMEWORK, 5))
    ),
    VERSATILE_FRAMEWORK_ALT_FLEXIBLE_FRAMEWORK(
        TransformerBuilding.MANUFACTURER,
        "[A] Flexible Framework",
        true,
        arrayOf(Rate(CraftableItem.MODULAR_FRAME, 3.75), Rate(CraftableItem.STEEL_BEAM, 22.5), Rate(CraftableItem.RUBBER, 30)),
        arrayOf(Rate(CraftableItem.VERSATILE_FRAMEWORK, 7.5))
    ),
    WATER_ALT_UNPACKAGE_WATER(
        TransformerBuilding.PACKAGER,
        "[A] Unpackage Water",
        true,
        arrayOf(Rate(CraftableItem.PACKAGED_WATER, 120)),
        arrayOf(Rate(RawItem.WATER, 120), Rate(CraftableItem.EMPTY_CANISTER, 120))
    ),
    WIRE(
        TransformerBuilding.CONSTRUCTOR,
        "[S] Wire ",
        false,
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 15)),
        arrayOf(Rate(CraftableItem.WIRE, 30))
    ),
    WIRE_ALT_CATERIUM_WIRE(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Caterium Wire",
        true,
        arrayOf(Rate(CraftableItem.CATERIUM_INGOT, 15)),
        arrayOf(Rate(CraftableItem.WIRE, 120))
    ),
    WIRE_ALT_FUSED_WIRE(
        TransformerBuilding.ASSEMBLER,
        "[A] Fused Wire",
        true,
        arrayOf(Rate(CraftableItem.COPPER_INGOT, 12), Rate(CraftableItem.CATERIUM_INGOT, 3)),
        arrayOf(Rate(CraftableItem.WIRE, 90))
    ),
    WIRE_ALT_IRON_WIRE(
        TransformerBuilding.CONSTRUCTOR,
        "[A] Iron Wire",
        true,
        arrayOf(Rate(CraftableItem.IRON_INGOT, 12.5)),
        arrayOf(Rate(CraftableItem.WIRE, 22.5))
    );

    override fun toString(): String {
        return "[$id" +
            " In: [${inputs.joinToString(" ") { it.toString() }}]" +
            " Out: [${outputs.joinToString(" ") { it.toString() }}]]"
    }

    companion object {
        fun getStandardFor(
            item: CraftableItem,
            avoidPackaged: Boolean = true,
            e: AltRecipeNotFoundException? = null
        ): TransformationRecipe {
            return values()
                .filter { !avoidPackaged || !it.inputs.any { t -> t.item.id.contains("PACKAGED") } }
                .filter { !avoidPackaged || !it.outputs.any { t -> t.item.id.contains("PACKAGED") } }
                .filter { it.name.contains(item.name) }
                .filter { !it.isAlternative }
                .find { it.outputs.find { rate -> rate.item == item } != null }
                ?: throw StandardRecipeNotFoundException(item.id, e)
        }

        fun getAltFor(
            item: CraftableItem,
            avoidPackaged: Boolean = true,
            e: StandardRecipeNotFoundException? = null
        ): TransformationRecipe {
            return values()
                .filter { !avoidPackaged || !it.inputs.any { t -> t.item.id.contains("PACKAGED") } }
                .filter { !avoidPackaged || !it.outputs.any { t -> t.item.id.contains("PACKAGED") } }
                .filter { it.name.contains(item.name) }
                .filter { it.isAlternative }
                .find { it.outputs.find { rate -> rate.item == item } != null }
                ?: throw AltRecipeNotFoundException(item.id, e)
        }

        fun getAny(item: CraftableItem): TransformationRecipe {
            return try {
                getAny(item, true)
            } catch (e: AltRecipeNotFoundException) {
                getAny(item, false)
            }
        }

        fun getAny(item: CraftableItem, avoidPackaged: Boolean = true): TransformationRecipe {
            return try {
                getStandardFor(item, avoidPackaged)
            } catch (e: StandardRecipeNotFoundException) {
                getAltFor(item, avoidPackaged, e)
            }
        }
    }
}
