package com.bancarelvalentin.smgg.logic.enums.building

@Suppress("unused")
enum class ExtratorBuilding(
    override val label: String,
    val solidOutputsCount: Int = 0,
    val fluidOutputsCount: Int = 0
) : Building {
    MINER("Miner", solidOutputsCount = 1),
    OIL_EXTRACTOR("Oil Extractor", fluidOutputsCount = 1),
    WATER_EXTRACTOR("Water extractor", fluidOutputsCount = 1),
    WELL_EXTRACTOR(" Well Extractors", fluidOutputsCount = 1),
}
