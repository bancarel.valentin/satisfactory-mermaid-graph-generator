package com.bancarelvalentin.smgg.logic.enums.recipe

import com.bancarelvalentin.smgg.logic.enums.building.ExtratorBuilding
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.model.RateOfOne

@Suppress("unused")
enum class ExtractionRecipe(
    override val building: ExtratorBuilding,
    override val outputs: Array<Rate>,
    override val label: String,
    override val inputs: Array<Rate> = emptyArray(),
    override val isAlternative: Boolean = false
) : Recipe {
    BAUXITE_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.BAUXITE)), "Bauxite mining"),
    CATERIUM_ORE_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.CATERIUM_ORE)), "Caterium ore mining"),
    COAL_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.COAL)), "Coal mining"),
    COPPER_ORE_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.COPPER_ORE)), "Copper ore mining"),
    IRON_ORE_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.IRON_ORE)), "Iron ore mining"),
    LIMESTONE_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.LIMESTONE)), "Limestone mining"),
    NITROGEN_WELL(ExtratorBuilding.WELL_EXTRACTOR, arrayOf(RateOfOne(RawItem.NITROGEN_GAS)), "Nitrogen gas well"),
    OIL_EXTRACTION(ExtratorBuilding.WATER_EXTRACTOR, arrayOf(RateOfOne(RawItem.CRUDE_OIL)), "Oil extractor"),
    OIL_WELL(ExtratorBuilding.WELL_EXTRACTOR, arrayOf(RateOfOne(RawItem.CRUDE_OIL)), "Oil well"),
    RAW_QUARTZ_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.RAW_QUARTZ)), "Raw quartz mining"),
    SAM_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.SAM_ORE)), "S.A.M ore  mining"),
    SULFUR_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.SULFUR)), "Sulfur  mining"),
    URANIUM_MINING(ExtratorBuilding.MINER, arrayOf(RateOfOne(RawItem.URANIUM)), "Uranium  mining"),
    WATER_EXTRACTION(ExtratorBuilding.WATER_EXTRACTOR, arrayOf(RateOfOne(RawItem.WATER)), "Water extractor"),
    WATER_WELL(ExtratorBuilding.WELL_EXTRACTOR, arrayOf(RateOfOne(RawItem.WATER)), "Water well");

    override fun toString(): String {
        return "[$id" +
            " Out: [${outputs.joinToString(" ") { it.toString() }}]]"
    }

    companion object {
        fun getAny(item: RawItem): ExtractionRecipe? {
            return values()
                .find { it.outputs.find { rate -> rate.item == item } != null }
        }
    }
}
