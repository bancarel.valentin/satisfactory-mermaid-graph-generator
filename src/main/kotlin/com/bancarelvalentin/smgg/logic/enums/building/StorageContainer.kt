package com.bancarelvalentin.smgg.logic.enums.building

@Suppress("unused")
class StorageContainer(
    override val label: String = "Storage container",
    val solidOutputsCount: Int = 2,
    val fluidOutputsCount: Int = 0
) : Building
