package com.bancarelvalentin.smgg.logic.enums.building

@Suppress("unused")
enum class TransformerBuilding(
    override val label: String,
    val solidInputsCount: Int = 0,
    val fluidInputsCount: Int = 0,
    val solidOutputsCount: Int = 0,
    val fluidOutputsCount: Int = 0,
    @Suppress("unused") val powerOutputsCount: Int = 0
) : Building {
    ASSEMBLER("Assembler", solidInputsCount = 2, solidOutputsCount = 1),
    BLENDER(
        "Blender",
        solidInputsCount = 2,
        fluidInputsCount = 2,
        solidOutputsCount = 1,
        fluidOutputsCount = 1
    ),
    COAL_GENERATOR(
        "Coal Generator",
        solidOutputsCount = 1,
        fluidInputsCount = 1,
        powerOutputsCount = 1
    ),
    CONSTRUCTOR("constructor", solidInputsCount = 1, solidOutputsCount = 1),
    FOUNDRY("Foundry", solidInputsCount = 2, solidOutputsCount = 1),
    FUEL_GENERATOR("Fuel Generator", fluidInputsCount = 1, powerOutputsCount = 1),
    GEOTHERMAL_GENERATOR("Geothermal Generator"),
    MANUFACTURER("Manufacturer", solidInputsCount = 4, solidOutputsCount = 1),
    NUCLEAR_POWER_PLANT(
        "Nuclear Power Plant",
        solidInputsCount = 1,
        fluidInputsCount = 1,
        solidOutputsCount = 1,
        powerOutputsCount = 1
    ),
    PACKAGER("Packager", solidInputsCount = 1, fluidInputsCount = 1, solidOutputsCount = 1, fluidOutputsCount = 1),
    PARTICLE_ACCELERATOR("Particle accelerator", solidInputsCount = 2, fluidInputsCount = 1, solidOutputsCount = 1),
    REFINERY("Refinery", solidInputsCount = 1, fluidInputsCount = 1, solidOutputsCount = 1, fluidOutputsCount = 1),
    SMELTER("Smelter", solidInputsCount = 1, solidOutputsCount = 1);
}
