package com.bancarelvalentin.smgg.logic.enums.recipe

import com.bancarelvalentin.smgg.NoSuchInputException
import com.bancarelvalentin.smgg.NoSuchOutputException
import com.bancarelvalentin.smgg.logic.enums.building.Building
import com.bancarelvalentin.smgg.logic.enums.item.Item
import com.bancarelvalentin.smgg.logic.model.Rate

interface Recipe {

    val building: Building
    val isAlternative: Boolean
    val inputs: Array<Rate>
    val outputs: Array<Rate>
    val label: String

    val id: String
        get() {
            return when (this) {
                is TransformationRecipe -> "RT__" + this.name
                is ExtractionRecipe -> "RE__" + this.name
                is UserManualInput -> "USER_INPUT"
                else -> throw RuntimeException()
            }
        }

    fun getInput(item: Item): Rate {
        return this.inputs.find { it.item == item } ?: throw NoSuchInputException(item)
    }

    fun getOutput(item: Item): Rate {
        return this.outputs.find { it.item == item } ?: throw NoSuchOutputException(item)
    }
}
