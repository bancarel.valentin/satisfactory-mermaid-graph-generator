package com.bancarelvalentin.smgg.logic.enums.item

@Suppress("unused")
enum class ItemState {
    SOLID,
    FUILD
}
