package com.bancarelvalentin.smgg.logic.enums.item

interface Item {

    val label: String
    val hasNoRecipe: Boolean
    val itemState: ItemState

    val id: String
        get() {
            return when (this) {
                is RawItem -> "R__" + this.name
                is CraftableItem -> "C__" + this.name
                else -> throw RuntimeException()
            }
        }

    fun isSameTypeAs(other: Any?): Boolean {
        return when (this) {
            is RawItem -> {
                when (other) {
                    is RawItem -> other.name == this.name
                    is CraftableItem -> other.name == this.name
                    else -> false
                }
            }
            is CraftableItem -> {
                when (other) {
                    is RawItem -> other.name == this.name
                    is CraftableItem -> other.name == this.name
                    else -> false
                }
            }
            else -> false
        }
    }
}
