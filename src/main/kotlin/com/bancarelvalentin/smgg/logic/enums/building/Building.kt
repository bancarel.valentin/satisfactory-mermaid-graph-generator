package com.bancarelvalentin.smgg.logic.enums.building

import com.bancarelvalentin.smgg.mermaid.MermaidDefinition
import com.bancarelvalentin.smgg.mermaid.Mermaidable
import com.bancarelvalentin.smgg.mermaid.enums.Shapes
import com.bancarelvalentin.smgg.theme.ThemeHolderManager

interface Building : Mermaidable {

    val label: String

    override val mermaidId: String
        get() {
            return when (this) {
                is ExtratorBuilding -> "E__" + this.name
                is TransformerBuilding -> "T__" + this.name
                is StorageContainer -> "STORAGE"
                else -> throw RuntimeException()
            }
        }

    override val mermaidLabel
        get() = label

    override val mermaidDefinition: MermaidDefinition
        get() {
            return MermaidDefinition(
                mermaidId,
                mermaidLabel,
                shape,
                className
            )
        }
    private val shape: Shapes?
        get() {
            return when (this) {
                is ExtratorBuilding -> ThemeHolderManager().EXTRACTORS_SHAPE
                is TransformerBuilding -> ThemeHolderManager().TRANSFORMERS_SHAPE
                is StorageContainer -> ThemeHolderManager().USER_INPUTS_SHAPE
                else -> throw RuntimeException()
            }
        }

    private val className: String?
        get() {
            return when (this) {
                is ExtratorBuilding -> ThemeHolderManager().EXTRACTORS_CLASS?.name ?: ThemeHolderManager().DEFAULT_CLASS_NAME
                is TransformerBuilding -> ThemeHolderManager().TRANSFORMERS_CLASS?.name ?: ThemeHolderManager().DEFAULT_CLASS_NAME
                is StorageContainer -> ThemeHolderManager().USER_INPUTS_CLASS?.name ?: ThemeHolderManager().DEFAULT_CLASS_NAME
                else -> throw RuntimeException()
            }
        }
}
