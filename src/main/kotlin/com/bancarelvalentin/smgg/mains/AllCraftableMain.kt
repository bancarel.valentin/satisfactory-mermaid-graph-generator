package com.bancarelvalentin.smgg.mains

import com.bancarelvalentin.smgg.Constants
import com.bancarelvalentin.smgg.logic.ProductionLineGenerator
import com.bancarelvalentin.smgg.logic.ProductionLinePrinter
import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.model.Rate

fun main() {
    val productionLineGenerator = ProductionLineGenerator()

    CraftableItem.values()
        .filter { !it.hasNoRecipe }
        .filter { !it.name.contains("PACKAGED") }
        .forEach { productionLineGenerator.addOutput(Rate(it, 10)) }
    RawItem.values()
        .filter { !it.hasNoRecipe }
        .forEach { productionLineGenerator.addOutput(Rate(it, 10)) }

    val graph = productionLineGenerator.generate()
    val examplesDir = Constants.DEFAULT_OUT_DIR.resolve("examples")
    examplesDir.toFile().mkdirs()
    ProductionLinePrinter(graph, "All craftable items").print(examplesDir.resolve("All craftables.md"))
}
