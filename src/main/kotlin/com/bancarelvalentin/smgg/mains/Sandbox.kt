package com.bancarelvalentin.smgg.mains

import com.bancarelvalentin.smgg.logic.ProductionLineGenerator
import com.bancarelvalentin.smgg.logic.ProductionLinePrinter
import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.model.Rate

fun main() {

    val graph = ProductionLineGenerator()
        .addOutput(Rate(CraftableItem.ALUMINA_SOLUTION, 360))
        .addOutput(Rate(CraftableItem.SILICA, 300))
        .generate()

    @Suppress("DEPRECATION")
    ProductionLinePrinter(arrayOf(graph)).sandboxPrint()
}
