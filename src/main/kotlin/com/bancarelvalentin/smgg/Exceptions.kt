package com.bancarelvalentin.smgg

import com.bancarelvalentin.smgg.logic.enums.item.Item

class UnknownItemType(java: Class<out Item>) : RuntimeException(java.simpleName)
class NoSuchInputException(item: Item) : RuntimeException(item.label)
class NoSuchOutputException(item: Item) : RuntimeException(item.label)
class StandardRecipeNotFoundException(name: String, cause: AltRecipeNotFoundException? = null) :
    RuntimeException(name, cause)

class AltRecipeNotFoundException(name: String, cause: StandardRecipeNotFoundException? = null) :
    RuntimeException(name, cause)

class TryingToAddToDifferentRatesException : Throwable()
class ComputeAlreadyDoneException : Throwable()
class AutoRecipeLoopException(cause: StackOverflowError) : RuntimeException(cause)
class UnknownException(cause: Exception) : RuntimeException(cause)
