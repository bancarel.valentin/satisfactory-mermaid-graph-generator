package com.bancarelvalentin.smgg

import java.nio.file.Paths

class Constants {
    companion object {
        val DEFAULT_OUT_DIR = Paths.get(".\\output\\")
    }
}
