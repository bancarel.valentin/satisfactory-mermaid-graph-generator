package com.bancarelvalentin.smgg.utils

import com.bancarelvalentin.smgg.theme.ThemeHolderManager
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.UUID

class StringUtils {
    companion object {
        fun tabs(count: Int): String {
            return repeatChar("\t", count)
        }

        fun formatDate(
            date: Date = Date(),
            dateFormat: DateFormat = SimpleDateFormat("yyyy-mm-dd hh:mm:ss.S")
        ): String {
            return dateFormat.format(date)
        }

        fun formatDecimalNumber(double: Double): String {
            return if (ThemeHolderManager().NO_ROUNDING!!) {
                return double.toString()
            } else {
                var format = "0."
                for (i in 1..ThemeHolderManager().ROUNDING_DECIMAL_PLACES!!) {
                    format += "#"
                }
                val df = DecimalFormat(format)
                df.roundingMode = ThemeHolderManager().ROUNDING_MODE!!
                df.format(double)
            }
        }

        fun randomUniqueString(): String {
            return "id_" + UUID.randomUUID().toString().replace("-", "")
        }

        fun snakeCase(str: String): String {
            return str
                .replace(Regex("([a-zA-Z])([A-Z])"), "$1 $2") // Split camel case words
                .replace(Regex("[^a-zA-Z0-9]"), " ") // Replace all non alphanum by spaces
                .replace(Regex("\\s+"), " ") // Remove double spaces
                .trim() // remove trailing and leading spaces
                .replace(Regex("\\W+"), "_") // Replace spaces by underscore
                .uppercase(Locale.getDefault()) // Uppercase
        }

        fun alphanumOnly(str: String): String {
            return str
                .replace(Regex("[^a-zA-Z0-9]"), " ") // Replace all non alphanum by spaces
                .replace(Regex("\\s+"), " ") // Remove double spaces
                .trim() // remove trailing and leading spaces
        }

        fun fsAllowedOnly(str: String): String {
            return str
                .replace(Regex("[\\\\/:*?\"<>|]"), " ") // Replace all non allowed by spaces
                .replace(Regex("\\s+"), " ") // Remove double spaces
                .trim() // remove trailing and leading spaces
        }

        fun repeatChar(str: String, count: Int): String {
            var ret = ""
            for (i in 1..count) {
                ret += str
            }
            return ret
        }
    }
}
