package com.bancarelvalentin.smgg.mermaid.enums

@Suppress("unused")
enum class Links {
    ARROW,
    OPEN,
    DOTTED,
    THICK;

    companion object {

        @Suppress("SameReturnValue")
        fun getDefault(): Links {
            return ARROW
        }
    }
}
