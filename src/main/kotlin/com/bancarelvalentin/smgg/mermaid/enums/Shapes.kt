package com.bancarelvalentin.smgg.mermaid.enums

@Suppress("unused")
enum class Shapes(val prefix: String, val suffix: String) {
    SQUARE("[", "]"),
    ROUNDED("(", ")"),
    STADIUM("([", "])"),
    SUBROUTINE("[[", "]]"),
    CYLINDRICAL("[(", ")]"),
    CIRCLE("((", "))"),
    ASSYMETRIC(">", "]"),
    RHOMBUS("{", "}"),
    HEXAGON("{{", "}}"),
    PARALLELOGRAM("[/", "/]"),
    PARALLELOGRAM_ALT("[\\", "\\]"),
    TRAPEZOID("[/", "\\]"),
    TRAPEZOID_ALT("[\\", "//]");

    companion object {
        @Suppress("SameReturnValue")
        fun getDefault(): Shapes {
            return SQUARE
        }
    }
}
