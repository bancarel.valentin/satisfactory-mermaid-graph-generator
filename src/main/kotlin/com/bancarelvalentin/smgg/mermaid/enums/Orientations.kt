package com.bancarelvalentin.smgg.mermaid.enums

@Suppress("unused")
enum class Orientations(val mermaidKeyword: String) {
    TOP_BOTTOM("TB"),
    TOP_DOWN("TD"),
    BOTTOM_TOP("BT"),
    RIGHT_LEFT("RL"),
    LEFT_RIGHT("LR");

    companion object {

        @Suppress("SameReturnValue")
        fun getDefault(): Orientations {
            return TOP_BOTTOM
        }
    }
}
