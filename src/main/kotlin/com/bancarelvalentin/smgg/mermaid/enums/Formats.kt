package com.bancarelvalentin.smgg.mermaid.enums

@Suppress("unused")
enum class Formats {
    MARKDOWN,
    MERMAID;

    companion object {
        @Suppress("SameReturnValue")
        fun getDefault(): Formats {
            return MARKDOWN
        }
    }
}
