package com.bancarelvalentin.smgg.mermaid

interface Mermaidable {

    val mermaidId: String
    val mermaidLabel: String
    val mermaidDefinition: MermaidDefinition
}
