package com.bancarelvalentin.smgg.mermaid

import com.bancarelvalentin.smgg.mermaid.enums.Shapes

class MermaidDefinition(val id: String, val label: String, shape: Shapes? = null, val className: String?) {

    val shape: Shapes = shape ?: Shapes.getDefault()

    override fun toString() = "$id${shape.prefix}\"${this.label}\"${shape.suffix}:::${className ?: "default"}"
}
