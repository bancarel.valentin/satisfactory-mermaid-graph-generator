package com.bancarelvalentin.smgg.mermaid

import com.bancarelvalentin.smgg.mermaid.enums.Links

class MermaidLine(
    val from: String,
    val to: String,
    linkType: Links? = null,
    val middle: String? = null
) {
    val linkType: Links = linkType ?: Links.getDefault()

    override fun toString() = "$from $link $to"

    private val link: String
        get() {

            return when (linkType) {
                Links.ARROW -> if (middle != null) "-- \"$middle\" -->" else "-->"
                Links.OPEN -> if (middle != null) "-- \"$middle\" ---" else "---"
                Links.DOTTED -> if (middle != null) "-. \"$middle\" .->" else "-.->"
                Links.THICK -> if (middle != null) "== \"$middle\" ==>" else "==>"
            }
        }
}
