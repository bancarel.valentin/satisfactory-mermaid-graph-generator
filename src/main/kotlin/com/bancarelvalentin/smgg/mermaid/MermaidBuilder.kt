package com.bancarelvalentin.smgg.mermaid

import com.bancarelvalentin.smgg.mermaid.enums.Formats
import com.bancarelvalentin.smgg.mermaid.enums.Orientations
import com.bancarelvalentin.smgg.utils.StringUtils

class MermaidBuilder(format: Formats?, val title: String? = null) {

    val format: Formats = format ?: Formats.getDefault()

    private val lines = ArrayList<String>()
    private var tabcount = 0

    private val tabs: String get() = StringUtils.tabs(tabcount)

    fun init(orientation: Orientations?): MermaidBuilder {
        if (format == Formats.MARKDOWN) {
            if (title != null) addLine("# $title")
            addLine("```mermaid")
            tabcount++
        }
        if (title != null) comment(title)
        addLine("graph ${(orientation ?: Orientations.getDefault()).mermaidKeyword}")
        tabcount++
        return this
    }

    private fun addLine(vararg strs: String) {
        lines.addAll(strs.map { "$tabs$it" })
    }

    fun breakline(): MermaidBuilder {
        addLine("")
        return this
    }

    fun comment(vararg comments: String): MermaidBuilder {
        addLine(*comments.map { "%% $it" }.toTypedArray())
        return this
    }

    fun addStyle(vararg styles: MermaidClass?): MermaidBuilder {
        addLine(*styles.filterNotNull().map { it.print() }.filterNotNull().toTypedArray())
        return this
    }

    fun define(vararg definitions: MermaidDefinition): MermaidBuilder {
        addLine(*definitions.map { it.toString() }.toTypedArray())
        return this
    }

    fun add(vararg items: MermaidLine): MermaidBuilder {
        addLine(*items.map { it.toString() }.toTypedArray())
        return this
    }

    fun build(): String {
        tabcount = 0
        if (format == Formats.MARKDOWN) {
            addLine("```")
        }
        breakline()
        breakline()
        return lines.joinToString("\n")
    }
}
