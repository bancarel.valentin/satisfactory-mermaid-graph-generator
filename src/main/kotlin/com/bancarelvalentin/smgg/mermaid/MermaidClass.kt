package com.bancarelvalentin.smgg.mermaid

import com.bancarelvalentin.smgg.theme.ThemeUpdatable
import com.bancarelvalentin.smgg.utils.StringUtils

class MermaidClass(
    var name: String? = StringUtils.randomUniqueString(),
    var fillColor: String?,
    var textColor: String?,
    var strokeColor: String?,
    var strokeWidth: Int?,
    var strokeDashes: Pair<Int, Int>?
) : ThemeUpdatable {

    fun print(): String? {
        val styles = ArrayList<String>()
        if (fillColor != null) {
            styles.add("fill:#$fillColor")
        }
        if (textColor != null) {
            styles.add("color:#$textColor")
        }
        if (strokeColor != null) {
            styles.add("stroke:#$strokeColor")
        }
        if (strokeWidth != null) {
            styles.add("stroke-width:${strokeWidth}px")
        }
        if (strokeDashes != null) {
            styles.add("stroke-dasharray:${strokeDashes!!.first} ${strokeDashes!!.second}")
        }

        return if (name != null && styles.isNotEmpty()) {
            "classDef $name ${styles.joinToString()};"
        } else {
            null
        }
    }

    override fun update(value: Any) {
        if (value is MermaidClass) {
            this.name = value.name ?: this.name
            this.fillColor = value.fillColor ?: this.fillColor
            this.textColor = value.textColor ?: this.textColor
            this.strokeColor = value.strokeColor ?: this.strokeColor
            this.strokeWidth = value.strokeWidth ?: this.strokeWidth
            this.strokeDashes = value.strokeDashes ?: this.strokeDashes
        }
    }
}
