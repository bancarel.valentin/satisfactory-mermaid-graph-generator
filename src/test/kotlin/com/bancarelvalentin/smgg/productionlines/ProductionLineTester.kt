package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.TEST_CONSTANTS
import com.bancarelvalentin.smgg.TestTheme
import com.bancarelvalentin.smgg.logic.ProductionLineGenerator
import com.bancarelvalentin.smgg.logic.ProductionLinePrinter
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.theme.ThemeHolderManager
import com.bancarelvalentin.smgg.utils.StringUtils
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.junit.Assert.assertThat
import java.text.DecimalFormat

class ProductionLineTester(
    val productionLine: ProductionLine,
    val clazz: Class<Any>,
    val preferedExtractionsMethod: Array<ExtractionRecipe> = emptyArray(),
    val preferedTransformationRecipes: Array<TransformationRecipe> = emptyArray()
) {
    fun doTests() {
        val dir = TEST_CONSTANTS.OUTPUT_DIR.resolve(StringUtils.snakeCase(clazz.simpleName))
        dir.toFile().mkdirs()

        // Computed
        val cmp = ProductionLineGenerator()
        val computed = cmp.addOutput(*productionLine.outputs.toTypedArray().copyOf())
            .preferTransformationRecipe(*preferedTransformationRecipes)
            .preferExtractionRecipe(*preferedExtractionsMethod)
            .generate()

        ThemeHolderManager.set(TestTheme(computed.nbNodes))

        // Print for debug
        val titleOriginal = "Original"
        val titleGenerated = "Computed"
        // print both in same file
        ProductionLinePrinter(arrayOf(productionLine, computed), titleOriginal, titleGenerated)
            .print(dir.resolve(TEST_CONSTANTS.OUTPUT_FILE_BOTH + ".md"))
        // then each in there own file
        ProductionLinePrinter(arrayOf(productionLine), titleOriginal)
            .print(dir.resolve(TEST_CONSTANTS.OUTPUT_FILE_ORIGINAL + ".md"))
        ProductionLinePrinter(arrayOf(computed), titleGenerated)
            .print(dir.resolve(TEST_CONSTANTS.OUTPUT_FILE_COMPUTED + ".md"))

        // Test user inputs
        productionLine.userInputs.values.forEach { originaluserInput ->
            val matchingGeneratedUserinput =
                computed.userInputs.values.find { originaluserInput.mermaidId == it.mermaidId }
            assertThat("Missing user chest ${originaluserInput.mermaidId}", matchingGeneratedUserinput, not(`is`(nullValue())))
            matchingGeneratedUserinput!!
            assertThat("Wrong overclock value for user chest  ${originaluserInput.mermaidId}", round(originaluserInput.overclockValue), equalTo(round(matchingGeneratedUserinput.overclockValue)))

            originaluserInput.recipe.inputs.forEach { originalInput ->
                val matchingGeneratedInput =
                    matchingGeneratedUserinput.recipe.inputs.find { originalInput.item == it.item }
                assertThat(
                    "Missing user chest input ${originalInput.item.id} for ${originaluserInput.mermaidId}",
                    matchingGeneratedInput, not(`is`(nullValue()))
                )
                matchingGeneratedInput!!
                assertThat(
                    "Wrong rate for input ${originalInput.item.id} for ${originaluserInput.mermaidId}",
                    round(originalInput.perMin),
                    equalTo(round(matchingGeneratedInput.perMin)),
                )
            }

            originaluserInput.recipe.outputs.forEach { originalOutput ->
                val matchingGeneratedOutput =
                    matchingGeneratedUserinput.recipe.outputs.find { originalOutput.item == it.item }
                assertThat(
                    "Missing user input output ${originalOutput.item.id} for ${originaluserInput.mermaidId}",
                    matchingGeneratedOutput, not(`is`(nullValue()))
                )
                matchingGeneratedOutput!!
                assertThat(
                    "Wrong rate for output ${originalOutput.item.id} for ${originaluserInput.mermaidId}",
                    round(originalOutput.perMin),
                    equalTo(round(matchingGeneratedOutput.perMin)),
                )
            }
        }

        // Test extractors
        productionLine.extractions.values.forEach { originalExtraction ->
            val matchingGeneratedExtractor =
                computed.extractions.values.find { originalExtraction.mermaidId == it.mermaidId }
            assertThat("Missing extraction ${originalExtraction.mermaidId}", matchingGeneratedExtractor, not(`is`(nullValue())))
            matchingGeneratedExtractor!!
            assertThat("Wrong overclock value for extraction  ${originalExtraction.mermaidId}", round(originalExtraction.overclockValue), equalTo(round(matchingGeneratedExtractor.overclockValue)))

            originalExtraction.recipe.inputs.forEach { originalInput ->
                val matchingGeneratedInput =
                    matchingGeneratedExtractor.recipe.inputs.find { originalInput.item == it.item }
                assertThat(
                    "Missing extractor input ${originalInput.item.id} for ${originalExtraction.mermaidId}",
                    matchingGeneratedInput, not(`is`(nullValue()))
                )
                matchingGeneratedInput!!
                assertThat(
                    "Wrong rate for input ${originalInput.item.id} for ${originalExtraction.mermaidId}",
                    round(originalInput.perMin),
                    equalTo(round(matchingGeneratedInput.perMin)),
                )
            }

            originalExtraction.recipe.outputs.forEach { originalOutput ->
                val matchingGeneratedOutput =
                    matchingGeneratedExtractor.recipe.outputs.find { originalOutput.item == it.item }
                assertThat(
                    "Missing extractor output ${originalOutput.item.id} for ${originalExtraction.mermaidId}",
                    matchingGeneratedOutput, not(`is`(nullValue()))
                )
                matchingGeneratedOutput!!
                assertThat(
                    "Wrong rate for output ${originalOutput.item.id} for ${originalExtraction.mermaidId}",
                    round(originalOutput.perMin),
                    equalTo(round(matchingGeneratedOutput.perMin))
                )
            }
        }

        // Test transformation
        productionLine.transformations.values.forEach { originalTransformation ->
            val matchingGeneratedTransformation =
                computed.transformations.values.find { originalTransformation.mermaidId == it.mermaidId }
            assertThat("Missing transformation ${originalTransformation.mermaidId}", matchingGeneratedTransformation, not(`is`(nullValue())))
            matchingGeneratedTransformation!!
            assertThat("Wrong overclock value for transformation  ${originalTransformation.mermaidId}", round(originalTransformation.overclockValue), equalTo(round(matchingGeneratedTransformation.overclockValue)))

            originalTransformation.recipe.inputs.forEach { originalInput ->
                val matchingGeneratedInput =
                    matchingGeneratedTransformation.recipe.inputs.find { originalInput.item == it.item }
                assertThat(
                    "Missing transformation input ${originalInput.item.id} for ${originalTransformation.mermaidId}",
                    matchingGeneratedInput, not(`is`(nullValue()))
                )
                matchingGeneratedInput!!
                assertThat(
                    "Wrong rate for input ${originalInput.item.id} for ${originalTransformation.mermaidId}",
                    round(originalInput.perMin),
                    equalTo(round(matchingGeneratedInput.perMin)),
                )
            }

            originalTransformation.recipe.outputs.forEach { originalOutput ->
                val matchingGeneratedOutput =
                    matchingGeneratedTransformation.recipe.outputs.find { originalOutput.item == it.item }
                assertThat(
                    "Missing transformation output ${originalOutput.item.id} for ${originalTransformation.mermaidId}",
                    matchingGeneratedOutput, not(`is`(nullValue()))
                )
                matchingGeneratedOutput!!
                assertThat(
                    "Wrong rate for output ${originalOutput.item.id} for ${originalTransformation.mermaidId}",
                    round(originalOutput.perMin),
                    equalTo(round(matchingGeneratedOutput.perMin)),
                )
            }
        }

        // Test raw items
        productionLine.rawResources.values.forEach { originalRaw ->
            val matchingGeneratedRaw = computed.rawResources.values.find { originalRaw.item == it.item }
            assertThat("Missing raw resource ${originalRaw.item.id}", matchingGeneratedRaw, not(`is`(nullValue())))
            matchingGeneratedRaw!!
            assertThat(
                "Wrong rate for raw resource  ${originalRaw.item.id}",
                round(originalRaw.perMin),
                equalTo(round(matchingGeneratedRaw.perMin)),
            )
        }

        // Test craftables items
        productionLine.intermediateItems.values.forEach { originalCraftable ->
            val matchingGeneratedCraftable =
                computed.intermediateItems.values.find { originalCraftable.item == it.item }
            assertThat("Missing craftable item ${originalCraftable.item.id}", matchingGeneratedCraftable, not(`is`(nullValue())))
            matchingGeneratedCraftable!!
            assertThat(
                "Wrong rate for craftable item ${originalCraftable.item.id}",
                round(originalCraftable.perMin),
                equalTo(round(matchingGeneratedCraftable.perMin)),
            )
        }

        // Test outputs
        productionLine.outputs.forEach { originalOutput ->
            val matchingGeneratedOutput = computed.outputs.find { originalOutput.item == it.item }
            assertThat("Missing ouutput ${originalOutput.item.id}", matchingGeneratedOutput, not(`is`(nullValue())))
            matchingGeneratedOutput!!
            assertThat(
                "Wrong rate for output ${originalOutput.item.id}",
                round(originalOutput.perMin),
                equalTo(round(matchingGeneratedOutput.perMin))
            )
        }

        // Test computed has no more data than original
        assertThat("Computed has more outputs than original", computed.outputs.size, equalTo(productionLine.outputs.size))
        assertThat("Computed has more rawResources than original", computed.rawResources.size, equalTo(productionLine.rawResources.size))
        assertThat("Computed has more intermediateItems than original", computed.intermediateItems.size, equalTo(productionLine.intermediateItems.size))
        assertThat("Computed has more extractions than original", computed.extractions.size, equalTo(productionLine.extractions.size))
        assertThat("Computed has more rawResources than original", computed.rawResources.size, equalTo(productionLine.rawResources.size))
    }

    private fun round(double: Double): Double {
        var format = "#."
        for (i in 1..TEST_CONSTANTS.ROUNDING_PRECISION) {
            format += "#"
        }
        val df = DecimalFormat(format)
        df.roundingMode = ThemeHolderManager().ROUNDING_MODE!!
        return df.format(double).toDouble()
    }
}
