package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.steps.Extraction
import com.bancarelvalentin.smgg.logic.steps.Transformation
import org.junit.jupiter.api.Test

internal class `0030_IronIngots` {

    private val RATE: Int = 8437

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(CraftableItem.IRON_INGOT, RATE))
        line.rawResources[RawItem.IRON_ORE] = Rate(RawItem.IRON_ORE, RATE)
        line.intermediateItems[CraftableItem.IRON_INGOT] = Rate(CraftableItem.IRON_INGOT, RATE)
        line.extractions[RawItem.IRON_ORE] = Extraction(ExtractionRecipe.IRON_ORE_MINING, RATE)
        line.transformations[CraftableItem.IRON_INGOT] = Transformation(TransformationRecipe.IRON_INGOT, RATE.toDouble() / 30.0)

        ProductionLineTester(line, this.javaClass).doTests()
    }
}
