package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.steps.Extraction
import com.bancarelvalentin.smgg.logic.steps.Transformation
import org.junit.jupiter.api.Test

internal class `0040_ReinforcedIronPlate` {

    private val RATE: Double = 27.5

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(CraftableItem.REINFORCED_IRON_PLATE, RATE))

        line.rawResources[RawItem.IRON_ORE] = Rate(RawItem.IRON_ORE, RATE * 12)

        line.intermediateItems[CraftableItem.IRON_INGOT] = Rate(CraftableItem.IRON_INGOT, RATE * 12)
        line.intermediateItems[CraftableItem.IRON_PLATE] = Rate(CraftableItem.IRON_PLATE, RATE * 6)
        line.intermediateItems[CraftableItem.IRON_ROD] = Rate(CraftableItem.IRON_ROD, RATE * 3)
        line.intermediateItems[CraftableItem.SCREW] = Rate(CraftableItem.SCREW, RATE * 12)
        line.intermediateItems[CraftableItem.REINFORCED_IRON_PLATE] = Rate(CraftableItem.REINFORCED_IRON_PLATE, RATE)

        line.extractions[RawItem.IRON_ORE] = Extraction(ExtractionRecipe.IRON_ORE_MINING, RATE * 12)

        line.transformations[CraftableItem.IRON_INGOT] = Transformation(TransformationRecipe.IRON_INGOT, RATE / 2.5)
        line.transformations[CraftableItem.IRON_PLATE] = Transformation(TransformationRecipe.IRON_PLATE, RATE / (3 + (1.0 / 3.0)))
        line.transformations[CraftableItem.IRON_ROD] = Transformation(TransformationRecipe.IRON_ROD, RATE / 5)
        line.transformations[CraftableItem.SCREW] = Transformation(TransformationRecipe.SCREW, RATE / (3 + (1.0 / 3.0)))
        line.transformations[CraftableItem.REINFORCED_IRON_PLATE] = Transformation(TransformationRecipe.REINFORCED_IRON_PLATE, RATE / 5)

        ProductionLineTester(line, this.javaClass).doTests()
    }
}
