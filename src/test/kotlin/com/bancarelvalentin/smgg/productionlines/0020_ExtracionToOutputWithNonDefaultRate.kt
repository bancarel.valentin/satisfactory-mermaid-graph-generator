package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.steps.Extraction
import org.junit.jupiter.api.Test

internal class `0020_ExtracionToOutputWithNonDefaultRate` {

    private val RATE: Double = 420.69

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(RawItem.IRON_ORE, RATE))
        line.rawResources[RawItem.IRON_ORE] = Rate(RawItem.IRON_ORE, RATE)
        line.extractions[RawItem.IRON_ORE] = Extraction(ExtractionRecipe.IRON_ORE_MINING, RATE)

        ProductionLineTester(line, this.javaClass).doTests()
    }
}
