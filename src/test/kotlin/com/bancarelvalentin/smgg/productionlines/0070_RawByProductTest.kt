package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import org.junit.jupiter.api.Test

internal class `0070_RawByProductTest` {

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(CraftableItem.ALUMINUM_SCRAP, 720))
        ProductionLineTester(line, this.javaClass, preferedTransformationRecipes = arrayOf(TransformationRecipe.ALUMINA_SOLUTION_ALT_SLOPPY_ALUMINA)).doTests()
    }
}
