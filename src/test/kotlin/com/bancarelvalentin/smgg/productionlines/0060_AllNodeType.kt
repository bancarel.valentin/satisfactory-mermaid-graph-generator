package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.UserManualInput
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.steps.Extraction
import com.bancarelvalentin.smgg.logic.steps.StorageInput
import com.bancarelvalentin.smgg.logic.steps.Transformation
import org.junit.jupiter.api.Test

internal class `0060_AllNodeType` {

    private val RATE: Double = 100.0

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(CraftableItem.COLOR_CARTRIDGE, RATE))
        line.outputs.add(Rate(CraftableItem.STEEL_INGOT, RATE))

        line.rawResources[RawItem.FLOWER_PETALS] = Rate(RawItem.FLOWER_PETALS, RATE / 2)
        line.rawResources[RawItem.IRON_ORE] = Rate(RawItem.IRON_ORE, RATE)
        line.rawResources[RawItem.COAL] = Rate(RawItem.COAL, RATE)

        line.intermediateItems[CraftableItem.COLOR_CARTRIDGE] = Rate(CraftableItem.COLOR_CARTRIDGE, RATE)
        line.intermediateItems[CraftableItem.STEEL_INGOT] = Rate(CraftableItem.COLOR_CARTRIDGE, RATE)

        line.userInputs[RawItem.FLOWER_PETALS] = StorageInput(UserManualInput(Rate(RawItem.FLOWER_PETALS, RATE / 2)), RATE / 2)

        line.extractions[RawItem.IRON_ORE] = Extraction(ExtractionRecipe.IRON_ORE_MINING, RATE)
        line.extractions[RawItem.COAL] = Extraction(ExtractionRecipe.COAL_MINING, RATE)

        line.transformations[CraftableItem.COLOR_CARTRIDGE] = Transformation(TransformationRecipe.COLOR_CARTRIDGE, RATE / 75)
        line.transformations[CraftableItem.STEEL_INGOT] = Transformation(TransformationRecipe.STEEL_INGOT, RATE / 45)

        ProductionLineTester(line, this.javaClass).doTests()
    }
}
