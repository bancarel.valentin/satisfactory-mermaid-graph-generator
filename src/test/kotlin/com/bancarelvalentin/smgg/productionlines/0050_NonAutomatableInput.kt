package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.enums.recipe.UserManualInput
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import com.bancarelvalentin.smgg.logic.steps.StorageInput
import com.bancarelvalentin.smgg.logic.steps.Transformation
import org.junit.jupiter.api.Test

internal class `0050_NonAutomatableInput` {

    private val RATE: Double = 100.0

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(CraftableItem.COLOR_CARTRIDGE, RATE))

        line.rawResources[RawItem.FLOWER_PETALS] = Rate(RawItem.FLOWER_PETALS, RATE / 2)

        line.intermediateItems[CraftableItem.COLOR_CARTRIDGE] = Rate(CraftableItem.COLOR_CARTRIDGE, RATE)

        line.userInputs[RawItem.FLOWER_PETALS] = StorageInput(UserManualInput(Rate(RawItem.FLOWER_PETALS, RATE / 2)), RATE / 2)

        line.transformations[CraftableItem.COLOR_CARTRIDGE] = Transformation(TransformationRecipe.COLOR_CARTRIDGE, RATE / 75)

        ProductionLineTester(line, this.javaClass).doTests()
    }
}
