package com.bancarelvalentin.smgg.productionlines

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.model.ProductionLine
import com.bancarelvalentin.smgg.logic.model.Rate
import org.junit.jupiter.api.Test

internal class `0080_CraftableByProductTest` {

    @Test
    fun `assert generated production line is ok`() {
        val line = ProductionLine()
        line.outputs.add(Rate(CraftableItem.ALUMINA_SOLUTION, 360))
        line.outputs.add(Rate(CraftableItem.SILICA, 300))
        ProductionLineTester(line, this.javaClass).doTests()
    }
}
