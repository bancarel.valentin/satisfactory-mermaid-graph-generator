package com.bancarelvalentin.smgg.theme

import org.hamcrest.CoreMatchers.isA
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.memberProperties

internal class ThemeTest {

    @Test
    fun `test that all overridable theme properties are nullable`() {
        Theme::class.memberProperties
            .filter { it.isOpen }
            .forEach {
                assertThat("${it.name} is not mutable", it as KMutableProperty<*>, isA(KMutableProperty::class.java))
                assert(it.returnType.isMarkedNullable) { "${it.name} is not nullable" }
            }
    }
}
