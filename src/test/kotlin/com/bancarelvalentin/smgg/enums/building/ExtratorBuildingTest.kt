package com.bancarelvalentin.smgg.enums.building

import com.bancarelvalentin.smgg.logic.enums.building.ExtratorBuilding
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test

internal class ExtratorBuildingTest {

    @Test
    fun `test that all enum entries are in alphabetical order`() {
        val values = ExtratorBuilding.values().map { it.name }
        val sortedCopy = ArrayList(values)
        sortedCopy.sort()
        values.forEachIndexed { index, value ->
            assertThat(
                "Enum not alphebeticaly sorted. Index#$index",
                sortedCopy[index],
                equalTo(value)
            )
        }
    }
}
