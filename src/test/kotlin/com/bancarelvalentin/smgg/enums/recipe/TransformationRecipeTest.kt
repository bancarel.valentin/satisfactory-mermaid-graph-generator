package com.bancarelvalentin.smgg.enums.recipe

import com.bancarelvalentin.smgg.logic.enums.item.ItemState
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.natpryce.hamkrest.greaterThanOrEqualTo
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

internal class TransformationRecipeTest {

    @Test
    fun `test that all enum entries are in alphabetical order`() {
        val values = TransformationRecipe.values().map { it.name }
        val sortedCopy = ArrayList(values)
        sortedCopy.sort()

        values.forEachIndexed { index, value ->
            assertThat(
                "Enum not alphebeticaly sorted. Index#$index",
                sortedCopy[index],
                equalTo(value),
            )
        }
    }

    @Test
    fun `test that no recipes have more input-output than it's building`() {
        TransformationRecipe.values().forEach {
            val iS = it.inputs.filter { it.item.itemState == ItemState.SOLID }.count()
            val iF = it.inputs.filter { it.item.itemState == ItemState.FUILD }.count()
            val oS = it.outputs.filter { it.item.itemState == ItemState.SOLID }.count()
            val oF = it.outputs.filter { it.item.itemState == ItemState.FUILD }.count()
            assertThat("${it.name}as more solid inputs ($iS) than it's machine (${it.building.name} - ${it.building.solidInputsCount})", it.building.solidInputsCount, `is`(greaterThanOrEqualTo(iS)))
            assertThat("${it.name}as more fluid inputs ($iF) than it's machine (${it.building.name} - ${it.building.fluidInputsCount})", it.building.fluidInputsCount, `is`(greaterThanOrEqualTo(iF)))
            assertThat("${it.name}as more solid outputs ($oS) than it's machine (${it.building.name} - ${it.building.solidOutputsCount})", it.building.solidOutputsCount, `is`(greaterThanOrEqualTo(oS)))
            assertThat("${it.name}as more fluid outputs ($oF) than it's machine (${it.building.name} - ${it.building.fluidOutputsCount})", it.building.fluidOutputsCount, `is`(greaterThanOrEqualTo(oF)))
        }
    }
}
