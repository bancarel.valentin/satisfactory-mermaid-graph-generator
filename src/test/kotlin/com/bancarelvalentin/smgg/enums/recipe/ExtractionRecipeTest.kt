package com.bancarelvalentin.smgg.enums.recipe

import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.jupiter.api.Test

internal class ExtractionRecipeTest {

    @Test
    fun `test that all enum entries are in alphabetical order`() {
        val values = ExtractionRecipe.values().map { it.name }
        val sortedCopy = ArrayList(values)
        sortedCopy.sort()
        values.forEachIndexed { index, value ->
            assertThat(
                "Enum not alphebeticaly sorted. Index#$index",
                sortedCopy[index],
                equalTo(value),
            )
        }
    }
}
