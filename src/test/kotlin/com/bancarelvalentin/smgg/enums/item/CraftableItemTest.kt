package com.bancarelvalentin.smgg.enums.item

import com.bancarelvalentin.smgg.logic.enums.item.CraftableItem
import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.TransformationRecipe
import com.bancarelvalentin.smgg.logic.model.Rate
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

internal class CraftableItemTest {

    @Test
    fun `test that all enum entries are in alphabetical order`() {
        val values = CraftableItem.values().map { it.name }
        val sortedCopy = ArrayList(values)
        sortedCopy.sort()
        values.forEachIndexed { index, value ->
            assertThat(
                "Enum not alphebeticaly sorted. Index#$index",
                sortedCopy[index],
                equalTo(value)
            )
        }
    }

    @Test
    fun `test no duplicate entry in CraftableItem and RawItems`() {
        CraftableItem.values().forEach { craft ->
            RawItem.values().forEach { raw ->
                assertThat("${craft.name} is both a raw resources and a craftable item", craft.name, not(equalTo(raw.name)))
            }
        }
    }

    @Test
    fun `test that all craftable items have a transformation recipe`() {
        val allRecipeOutput = TransformationRecipe.values().flatMap { it.outputs.map { se: Rate -> se.item.id } }
        CraftableItem.values()
            .filter { !it.hasNoRecipe }
            .forEach {
                assertThat("${it.id} has no recipe to be crafted from", allRecipeOutput, hasItem(it.id))
            }
    }
}
