package com.bancarelvalentin.smgg.enums.item

import com.bancarelvalentin.smgg.logic.enums.item.RawItem
import com.bancarelvalentin.smgg.logic.enums.recipe.ExtractionRecipe
import com.bancarelvalentin.smgg.logic.model.Rate
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

internal class RawItemTest {

    @Test
    fun `test that all enum entries are in alphabetical order`() {
        val values = RawItem.values().map { it.name }
        val sortedCopy = ArrayList(values)
        sortedCopy.sort()
        values.forEachIndexed { index, value ->
            assertThat(
                "Enum not alphebeticaly sorted. Index#$index",
                sortedCopy[index],
                equalTo(value),
            )
        }
    }

    @Test
    fun `test that all raw items have an extraction recipe`() {
        val allRecipeOutput = ExtractionRecipe.values().flatMap { it.outputs.map { se: Rate -> se.item.id } }
        RawItem.values()
            .filter { !it.hasNoRecipe }
            .forEach {
                assertThat("${it.id} has no recipe to be extracted from", allRecipeOutput, hasItem(it.id))
            }
    }
}
