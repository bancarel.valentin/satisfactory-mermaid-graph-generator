package com.bancarelvalentin.smgg

import java.nio.file.Path

class TEST_CONSTANTS {
    companion object {
        val OUTPUT_DIR: Path = Constants.DEFAULT_OUT_DIR.resolve("test")
        val OUTPUT_FILE_BOTH = "both"
        val OUTPUT_FILE_ORIGINAL = "original"
        val OUTPUT_FILE_COMPUTED = "computed"
        val ROUNDING_PRECISION = 3
        val MAX_NODES_BEFORE_ROTATING = 20
    }
}
