package com.bancarelvalentin.smgg

import com.bancarelvalentin.smgg.mermaid.enums.Orientations
import com.bancarelvalentin.smgg.theme.themes.ColorfullTheme

class TestTheme(nbNodes: Int) : ColorfullTheme() {
    override var ORIENTATION: Orientations? = if (nbNodes > TEST_CONSTANTS.MAX_NODES_BEFORE_ROTATING) Orientations.LEFT_RIGHT else Orientations.TOP_BOTTOM
}
